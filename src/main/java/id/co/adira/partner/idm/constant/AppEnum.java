package id.co.adira.partner.idm.constant;

public enum AppEnum {
    DATA_NOT_FOUND("Data Not Found"),
    USER_IS_NOT_REGISTERED_YET("User Belum Terdaftar"),
    FAILED_UPDATE_PROFILE("`Gagal Update Profile "),
    FAILED_GET_USER_BY_ID("Failed Get User By Id {}"),
    FAILED_CHANGE_PASSWORD("Failed Change Password "),
    FAILED_RESET_PASSWORD("Failed Reset Password "),
    FAILED_AUTH("Failed Auth "),
    UPDATE_SUCCESS("Update Success"),
    OK("OK"),
    ERROR("Error"),
    AUTH_FIRST_LOGIN("firstLogin"),
    AUTH_LOGIN("login"),
    AUTH_TOKEN("token"),
    URL_NOT_FOUND("Can't found your specified URL: "),
    FIND_USER_BY_ID("Find user by id: "),
    UPDATE_PROFILE_WITH_ID("Update profile with id: "),
    MENU_ID_PREFIX("MNU"),
    ROLE_ID_PREFIX("ROL"),
    USER_ID_PREFIX("USR"),
    ONE_TO_ZERO("1234567890"),
    SYSTEM("SYSTEM"),
    AXI("AXI"),
    KEDAY("KDY"),
    ADMIN("Admin"),
    OBJT_DESC("OBJT_DESC"),
    CONTENT_TYPE("Content-Type"),
    SELECT_ONE("<--SELECT ONE-->");

    private String message;

    AppEnum(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
