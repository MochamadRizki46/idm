package id.co.adira.partner.idm.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum DateFormatEnum {

    DAY_MONTH_YEAR_FORMAT("dd-MM-yyyy");

    private String message;
}
