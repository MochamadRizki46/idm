package id.co.adira.partner.idm.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum LoggerEnum {
    ERROR_WHILE_SAVING_PIC_TO_DB("Error while saving PIC to db: {}"),
    ERROR_WHILE_GETTING_MENU_FROM_DB("Error while getting menu from db: {}"),
    SUCCESS_GETTING_MENU_FROM_DB("Success getting menu from db. Data: {}"),
    ERROR_WHILE_GETTING_PIC_FROM_DB("Error while getting pic from db: {}"),
    ERROR_WHILE_GETTING_ROLE_FROM_DB("Error while getting role from DB: {}"),
    SUCCESS_GETTING_PIC_FROM_DB("Success getting PIC from db. Data: {}"),
    SUCCESS_GETTING_ROLE_FROM_DB("Success getting role from DB. Data: {}"),
    ERROR_WHILE_NON_ACTIVING_PIC("Error while non-activing one of PIC: {}"),
    ERROR_WHILE_GENERATING_ID("Error while generating id: {}"),
    ERROR_WHILE_INSERTING_OR_UPDATING_MENU("Error while inserting or updating menu to DB: {}"),
    ERROR_WHILE_INSERTING_OR_UPDATING_ROLE("Error while inserting or updating role to DB: {}"),
    SUCCESS_SAVING_NEW_MENU_INTO_DB("Success saving new menu into DB. Data: {}"),
    SUCCESS_SAVING_NEW_ROLE_INTO_DB("Success saving new role into DB. Data: {}"),
    ERROR_WHILE_UPDATING_STATUS_ROLE("Error while updating status role from DB: {}"),
    SUCCESS_UPDATING_ROLE("Success updating role from DB. Data: {}"),
    ERROR_WHILE_SHOWING_DETAIL_PIC("Error while showing detail PIC: {}"),
    ERROR_WHILE_SHOWING_DETAIL_MENU("Error while showing detail menu: {}"),
    ERROR_WHILE_SHOWING_DETAIL_ROLE("Error while showing detail role: {}"),
    SUCCESS_SHOWING_DETAIL_PIC("Success showing detail PIC. Data: {}"),
    SUCCESS_SHOWING_DETAIL_MENU("Success showing detail menu. Data: {}"),
    SUCCESS_SHOWING_DETAIL_ROLE("Success showing detail role. Data: {}"),
    ERROR_WHILE_SEARCHING_PIC_BY_NAME("Error while searching PIC by name: {}"),
    SUCCESS_SEARCHING_PIC_BY_NAME("Success searching PIC by name. Data: {}"),
    SUCCESS_INSERT_USER_DETAIL_MENU("Success inserting user with all provided menu. Data: {}"),
    SUCCESS_UPDATE_MENU("Success updating an existing menu: {}"),
    ERROR_WHILE_GETTING_ELIGIBLE_MENU_FOR_USER("Error while getting eligible menu for user: {}"),
    SUCCESS_GETTING_ELIGIBLE_MENU_FOR_USER("Success getting eligible menu for user. Data: {}"),
    ERROR_WHILE_UPDATING_MENU_STATUS("Error while updating menu's status: {}"),
    ERROR_WHILE_GENERATING_USER_ID("Error while generating user id: {}"),
    SUCCESS_INSERTING_PARA_USER_DETAIL_ROLE_TO_DB("Successfully inserting para user detail and role into connection table. Data: {}"),
    SUCCESS_UPDATING_PARA_USER_DETAIL_ROLE_FROM_DB("Successfully updating para user detail role from db. Data: {}"),
    ERROR_WHILE_DELETING_ROLE("Error while deleting role: {}"),
    ERROR_WHILE_DELETING_MENU("Error while deleting menu: {}"),
    ERROR_WHILE_DELETING_PIC("Error while deleting PIC: {}"),
    SUCCESS_DELETING_ROLE("Successfully deleting role from DB!"),
    SUCCESS_DELETING_MENU("Successfully deleting menu from DB!"),
    ERROR_WHILE_INSERTING_NEW_PIC("Error while inserting new pic into db: {}"),
    ERROR_WHILE_INSERTING_NEW_MENU("Error while inserting new menu into db: {}"),
    ERROR_WHILE_INSERTING_NEW_ROLE("Error while inserting new role into db: {}"),
    SUCCESS_INSERTING_NEW_PIC("Success inserting new pic into db. Data: {}"),
    SUCCESS_INSERTING_NEW_ROLE("Success inserting new role into db. Data: {}"),
    SUCCESS_INSERTING_NEW_MENU("Success inserting new menu into db. Data: {}"),
    SUCCESS_DELETING_PIC("Successfully deleting PIC from DB!"),
    ERROR_WHILE_GET_DATA("ERROR WHILE GET DATA");

    private String message;

}
