package id.co.adira.partner.idm.constant;

public enum StatusEnum {
    FAILED_UPDATE_OR_SAVE_DATA_LOGIN("Failed Update Or Save Data Login: "),
    USER_NOT_FOUND_WITH_USERNAME("Can't find user with username "),
    USER_NOT_FOUND_WITH_ID("Can't find user with id "),
    OWNER_DEALER_NOT_FOUND("Owner Dealer not found!"),
    ADMIN_DEALER_NOT_FOUND("Admin Dealer not found!"),
    SUCCESS_SAVING_DATA_TO_DB("Success saving data to db!"),
    USER_ID_ALREADY_EXISTS("User ID already exists!"),
    ROLE_ID_ALREADY_EXISTS("Role ID already exists!"),
    SUCCESS_UPDATING_DATA("Success updating a data!"),
    INVALID_ROLE_CODE("Invalid Role Code!"),
    INVALID_MENU_CODE("Invalid Menu Code!"),
    NO_MENU_LIST_IN_OUR_DATABASE("No Menu List on Our Database!"),
    CANNOT_FIND_ROLE("Can't find a role with role code: "),
    CANNOT_FIND_MENU("Can't find a menu with menu code: "),
    SEARCHED_PIC_NAME_MUST_BE_AT_LEAST_THREE_CHARACTER("Searched PIC Name must be at least 3 characters!"),
    THERE_IS_SOMETHING_ERROR_IN_OUR_SERVER("There's something error in our server! Please contact our developer for more info...");

    private String message;

    StatusEnum(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
