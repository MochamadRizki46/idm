	package id.co.adira.partner.idm.controller;

import id.co.adira.partner.idm.dto.*;
import id.co.adira.partner.idm.service.AdminService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import javax.validation.Valid;

@RestController
@RequestMapping("api/admin")
public class AdminController {

	private final AdminService adminService;

	@Autowired
	public AdminController(AdminService adminService) {
		this.adminService = adminService;
	}


    @PostMapping("/update-pic")
    @ApiOperation(value = "Update PIC", nickname = "Insert or Update PIC")
    //@CrossOrigin
    public ResponseEntity<Object> insertOrUpdatePic(@RequestBody PicRequestDTO picRequestDTO){
        BaseResponse response = adminService.insertOrUpdatePic(picRequestDTO);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }

	@PostMapping("/show-all-menu")
	@ApiOperation(value = "Show All Menu", nickname = "Show All Menu")
	//@CrossOrigin
	public ResponseEntity<Object> showAllMenu(@RequestBody @Valid FilterPageAndDataDTO filterPageAndDataDTO) {
		BaseResponse response = adminService.showAllMenu(filterPageAndDataDTO);
		return new ResponseEntity<>(response, response.getHttpStatus());
	}

	@PostMapping("/show-all-pic")
	@ApiOperation(value = "Show All PIC", nickname = "Show All PIC")
	//@CrossOrigin
	public ResponseEntity<Object> showAllPic(@RequestBody @Valid FilterPageAndDataDTO filterPageAndDataDTO) {
		BaseResponse response = adminService.showAllPic(filterPageAndDataDTO);
		return new ResponseEntity<>(response, response.getHttpStatus());
	}

	@PostMapping("/insert-or-update-menu")
	@ApiOperation(value = "Insert or Update Menu", nickname = "Insert or Update Menu")
	//@CrossOrigin
	public ResponseEntity<Object> insertOrUpdateMenu(@RequestBody AdminMenuRequestDTO adminMenuRequestDTO) {
		BaseResponse response = adminService.insertOrUpdateMenu(adminMenuRequestDTO);
		return new ResponseEntity<>(response, response.getHttpStatus());

	}

    @PostMapping("/update-role")
    @ApiOperation(value = "Insert or Update Role", nickname = "Insert or Update Role")
    //@CrossOrigin
    public ResponseEntity<Object> insertOrUpdateRole(@RequestBody RoleRequestDTO roleRequestDTO){
        BaseResponse response = adminService.insertOrUpdateRole(roleRequestDTO);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }

	@GetMapping("/generate-menu-id")
	@ApiOperation(value = "Generate Menu Id", nickname = "Generate Menu Id")
	//@CrossOrigin
	public ResponseEntity<Object> generateMenuId() {
		BaseResponse response = adminService.generateMenuId();
		return new ResponseEntity<>(response, response.getHttpStatus());
	}

	@GetMapping("/generate-role-id")
	@ApiOperation(value = "Generate Role Id", nickname = "Generate Role Id")
	//@CrossOrigin
	public ResponseEntity<Object> generateRoleId() {
		BaseResponse response = adminService.generateRoleId();
		return new ResponseEntity<>(response, response.getHttpStatus());
	}

	@GetMapping("/generate-user-id")
	@ApiOperation(value = "Generate User Id", nickname = "Generate User Id")
	//@CrossOrigin
	public ResponseEntity<Object> generateUserId() {
		BaseResponse response = adminService.generateUserId();
		return new ResponseEntity<>(response, response.getHttpStatus());
	}

	@PostMapping("/show-all-role")
	@ApiOperation(value = "Show All Role", nickname = "Show All Role")
	//@CrossOrigin
	public ResponseEntity<Object> showAllRole(@RequestBody @Valid FilterPageAndDataDTO filterPageAndDataDTO) {
		BaseResponse response = adminService.showAllRole(filterPageAndDataDTO);
		return new ResponseEntity<>(response, response.getHttpStatus());

	}

	@PostMapping("/update-status-role")
	@ApiOperation(value = "Update Status Role", nickname = "Update Status Role")
	//@CrossOrigin
	public ResponseEntity<Object> updateStatusRole(@RequestParam String roleId) {
		BaseResponse response = adminService.updateStatusRole(roleId);
		return new ResponseEntity<>(response, response.getHttpStatus());
	}

	@PostMapping("/update-status-pic")
	@ApiOperation(value = "Update Status PIC", nickname = "Update Status PIC")
	//@CrossOrigin
	public ResponseEntity<Object> updateStatusPic(@RequestParam String userId) {
		BaseResponse response = adminService.updateStatusPic(userId);
		return new ResponseEntity<>(response, response.getHttpStatus());
	}

	@GetMapping("/show-detail-menu")
	@ApiOperation(value = "Show Detail Menu", nickname = "Show Detail Menu")
	//@CrossOrigin
	public ResponseEntity<Object> showDetailMenuBasedOnMenuCode(@RequestParam String menuCode) {
		BaseResponse response = adminService.showDetailMenuBasedOnMenuCode(menuCode);
		return new ResponseEntity<>(response, response.getHttpStatus());
	}

	@PostMapping("/show-detail-pic")
	@ApiOperation(value = "Show Detail PIC", nickname = "Show Detail PIC")
	//@CrossOrigin
	public ResponseEntity<Object> showDetailPicBasedOnUserCode(@RequestBody DetailPicRequestDTO detailPicRequestDTO) {
		BaseResponse response = adminService.showDetailPicByUserId(detailPicRequestDTO);
		return new ResponseEntity<>(response, response.getHttpStatus());
	}

	@GetMapping("/show-detail-role")
	@ApiOperation(value = "Show Detail Role", nickname = "Show Detail Role")
	//@CrossOrigin
	public ResponseEntity<Object> showDetailRoleBasedOnRoleCode(@RequestParam String roleId) {
		BaseResponse response = adminService.showDetailRoleBasedOnRoleCode(roleId);
		return new ResponseEntity<>(response, response.getHttpStatus());
	}

	@GetMapping("/search-pic-by-name")
	@ApiOperation(value = "Search PIC by Name", nickname = "Search PIC by Name")
	//@CrossOrigin
	public ResponseEntity<Object> searchPicByName(@RequestParam String picName) {
		BaseResponse response = adminService.searchPicByName(picName);
		return new ResponseEntity<>(response, response.getHttpStatus());
	}

	@PostMapping("/update-status-menu")
	@ApiOperation(value = "Update Status Menu", nickname = "Update Status Menu")
	//@CrossOrigin
	public ResponseEntity<Object> updateStatusMenu(@RequestParam String menuCode) {
		BaseResponse response = adminService.updateStatusMenu(menuCode);
		return new ResponseEntity<>(response, response.getHttpStatus());
	}

    @PostMapping("/insert-new-pic")
    @ApiOperation(value = "Insert New PIC", nickname = "Insert New PIC")
    //@CrossOrigin
    public ResponseEntity<Object> insertNewPic(@RequestBody PicRequestDTO picRequestDTO){
        BaseResponse baseResponse = adminService.insertNewPic(picRequestDTO);
        return new ResponseEntity<>(baseResponse, baseResponse.getHttpStatus());
    }

    @PostMapping("/insert-new-role")
    @ApiOperation(value = "Insert New Role", nickname = "Insert New Role")
    //@CrossOrigin
    public ResponseEntity<Object> insertNewRole(@RequestBody RoleRequestDTO roleRequestDTO){
        BaseResponse baseResponse = adminService.insertNewRole(roleRequestDTO);
        return new ResponseEntity<>(baseResponse, baseResponse.getHttpStatus());
    }

    @PostMapping("/delete-menu")
    @ApiOperation(value = "Delete Menu by Menu Code", nickname = "Delete Menu by Menu Code")
    //@CrossOrigin
    public ResponseEntity<Object> deleteMenuByMenuCode(@RequestParam String menuCode){
        BaseResponse response = adminService.deleteMenuByMenuCode(menuCode);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }

	@PostMapping("/delete-pic")
	@ApiOperation(value = "Delete PIC by Id", nickname = "Delete PIC by Id")
	//@CrossOrigin
	public ResponseEntity<Object> deletePicById(@RequestParam Integer id) {
		BaseResponse response = adminService.deletePicById(id);
		return new ResponseEntity<>(response, response.getHttpStatus());
	}

	@PostMapping("/delete-role")
	@ApiOperation(value = "Delete Role by Role Id", nickname = "Delete Role by Role Id")
	//@CrossOrigin
	public ResponseEntity<Object> deleteRoleByRoleId(@RequestParam String roleId) {
		BaseResponse response = adminService.deleteRoleByRoleId(roleId);
		return new ResponseEntity<>(response, response.getHttpStatus());
	}

	@PostMapping("/insert-user-data")
	@ApiOperation(value = "Insert User Data", nickname = "Insert User Data")
	//@CrossOrigin
	public ResponseEntity<Object> inserUserData(@RequestBody List<UserDataInjectDTO> insertDataInject) {
		BaseResponse response = adminService.insertDataUser(insertDataInject);
		return new ResponseEntity<>(response, response.getHttpStatus());
	}

	@GetMapping("/get-partner-type")
	@ApiOperation(value = "Get Partner Type", nickname = "Get Partner Type")
	//@CrossOrigin
	public ResponseEntity<Object> getPartnerType() {
		BaseResponse response = adminService.getPartnerType();
		return new ResponseEntity<>(response, response.getHttpStatus());
	}

	@GetMapping("/get-role-by-partnertype")
	@ApiOperation(value = "Get Role By Partner Type", nickname = "Get Role By Partner Type")
	//@CrossOrigin
	public ResponseEntity<Object> getRoleByPartnerType(@RequestParam String partnerType) {
		BaseResponse response = adminService.getRoleByPartnerType(partnerType);
		return new ResponseEntity<>(response, response.getHttpStatus());

	}
}
