package id.co.adira.partner.idm.controller;

import id.co.adira.partner.idm.dto.VersionDTO;
import id.co.adira.partner.idm.service.IDMService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/url/v1")
public class PartnerUrlController {

    @Autowired
    IDMService idmService;

    @GetMapping("/get-info-apps")
    //@CrossOrigin
    public VersionDTO getInfoApps(){
        VersionDTO getInfoAppsResponse = idmService.getInfoApps();
        return getInfoAppsResponse;
    }
}
