package id.co.adira.partner.idm.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import id.co.adira.partner.idm.dto.*;
import id.co.adira.partner.idm.exception.AdiraCustomException;
import id.co.adira.partner.idm.service.IDMService;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static id.co.adira.partner.idm.constant.AppEnum.*;

import java.util.Map;

@RestController
@RequestMapping("api/users")
public class UserController {

	@Autowired
	private IDMService idmService;
	//v01

	private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);
	
	int status = 1;
	String appversion = "v1.0.0";

	@PostMapping("/authUser")
	@ApiOperation(value = "Authenticate User", nickname = "Authenticate User")
	//@CrossOrigin
	public ResponseEntity<Object> authLoginV1(@RequestBody UserLoginRequest request) throws AdiraCustomException, JsonProcessingException {
		LOGGER.info("user auth login");
		String msg = OK.getMessage();
		StandardResponseDTO responseLogin = new StandardResponseDTO();
		UserDetailDTO responseDetail = new UserDetailDTO();

		try {
			responseLogin = idmService.authUser(request);
		} catch (AdiraCustomException e) {
			LOGGER.info("Error while hitting login API: {}", e.getMessage());
		}

		if (responseLogin.getStatus() == 1) {
			responseDetail = idmService.findUserByIdV1(request.getLogin());

			if (responseDetail.getUserId() == null) {
				status = 0;
				msg = USER_IS_NOT_REGISTERED_YET.getMessage();
			} else {
				Map<String, Object> resulty = idmService.checkData(request.getLogin());
				LOGGER.info(resulty.get(AUTH_LOGIN.toString()).toString());
				LOGGER.info(resulty.get(AUTH_TOKEN.toString()).toString());

			}
		} else {
			status = 0;
			msg = responseLogin.getMessage();
			
		}

		return ResponseEntity.ok(new AuthResponseDTO<>(status, msg, appversion, responseDetail));
	}

	@PostMapping("/resetPassword")
	@ApiOperation(value = "Reset Password", nickname = "Reset Password")
	//@CrossOrigin
	public StandardResponseDTO resetPassword(@RequestBody ResetPasswordRequest request) throws AdiraCustomException {
		LOGGER.info("Reset Password");
		StandardResponseDTO response = new StandardResponseDTO();

		try {
			response = idmService.resetPassword(request);
		} catch (AdiraCustomException e) {
			e.getMessage();
		}

		return response;
	}

	@PostMapping("/changePassword")
	@ApiOperation(value = "Change Password", nickname = "Change Password")
	//@CrossOrigin
	public StandardResponseDTO changePassword(@RequestBody ChangePasswordRequest request) throws AdiraCustomException {
		LOGGER.info("Reset Password");
		StandardResponseDTO response = new StandardResponseDTO();

		try {
			response = idmService.changePassword(request);
		} catch (AdiraCustomException e) {
			e.getMessage();
		}

		return response;
	}

	@PostMapping("/getUserProperty")
	@ApiOperation(value = "Get User Property", nickname = "Get User Property")
	//@CrossOrigin
	public ResponseEntity<Object> getUserPropertyByIdV1(@RequestBody UserDetailRequest request)
			throws AdiraCustomException {
		LOGGER.info("user property");
		String msg = OK.getMessage();
		UserDetailDTO response = new UserDetailDTO();

		try {
			response = idmService.findUserByIdV1(request.getUserId());
		} catch (AdiraCustomException e) {
			LOGGER.info(e.getMessage());
		}

		LOGGER.info(response.getUserId());
		if (response.getUserId() == null) {
			status = 0;
			msg = DATA_NOT_FOUND.getMessage();
		}
		return ResponseEntity.ok(new ResponseDTO(status, msg, response));
	}

	@PostMapping("/updateProfile")
	@ApiOperation(value = "Update Profile", nickname = "Update Profile")
	//@CrossOrigin
	public ResponseEntity<Object> updateProfile(@RequestBody UpdateProfileDTO updateProfileRequest) {

		LOGGER.info("update profile");
		var status = 0;
		var msg = ERROR.getMessage();
		String response = "";

		try {
			response = idmService.updateProfile(updateProfileRequest);
		} catch (AdiraCustomException e) {
			e.getMessage();
		}

		if (response.equalsIgnoreCase(UPDATE_SUCCESS.getMessage())) {
			status = 1;
			msg = UPDATE_SUCCESS.getMessage();
		} else if (response.equalsIgnoreCase(DATA_NOT_FOUND.getMessage())) {
			status = 0;
			msg = DATA_NOT_FOUND.getMessage();
		}
		return ResponseEntity.ok(new ResponseDTO(status, msg, updateProfileRequest.getUserId()));
	}

	@PostMapping("/v2/authUser")
	@ApiOperation(value = "Authenticate User V2", nickname = "Authenticate User V2")
	//@CrossOrigin
	public ResponseEntity<Object> authLoginV2(@RequestBody UserLoginRequest request) throws AdiraCustomException, JsonProcessingException {
		ResponseDTO responseDTO = new ResponseDTO();
		StandardResponseDTO standardResponseDTO = idmService.authUser(request);

		if(standardResponseDTO.getStatus() == 1) {
			UserDetailRequest userDetailRequest = new UserDetailRequest();
			userDetailRequest.setUserId(request.getLogin());
			responseDTO = idmService.getUserPropertyV3(userDetailRequest);
		}
		else
			insertResponseWhenStatusIsZero(responseDTO, standardResponseDTO);

		return new ResponseEntity(responseDTO, HttpStatus.OK);
	}

	private void insertResponseWhenStatusIsZero(ResponseDTO responseDTO, StandardResponseDTO standardResponseDTO) {
		responseDTO.setStatus(standardResponseDTO.getStatus());
		responseDTO.setMessage(standardResponseDTO.getMessage());
		responseDTO.setData(new UserDetailDTOV3());
	}

	@PostMapping("/v2/getUserProperty")
	@ApiOperation(value = "Get User Property V2", nickname = "Get User Property V2")
	//@CrossOrigin
	public ResponseEntity<Object> getUserPropertyByIdV2(@RequestBody UserDetailRequest request) {
		ResponseDTO responseDTO = idmService.getUserPropertyV3(request);
		return new ResponseEntity<>(responseDTO, HttpStatus.OK);
	}

	@PostMapping("/get-eligible-menu")
	@ApiOperation(value = "Get Eligible Menu by User Id", nickname = "Get Eligible Menu by User Id")
	//@CrossOrigin
	public ResponseEntity<Object> getEligibleMenuByUserId(@RequestBody UserDetailRequest userDetailRequest){
		ResponseDTO responseDTO = idmService.getEligibleMenuByUserId(userDetailRequest.getUserId());
		return new ResponseEntity<>(responseDTO, HttpStatus.OK);

	}
}
