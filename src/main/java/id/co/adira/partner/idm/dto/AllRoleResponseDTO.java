package id.co.adira.partner.idm.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AllRoleResponseDTO {
    private String partnerType;
    private String roleId;
    private String roleName;
    private Integer active;

}
