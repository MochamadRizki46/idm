package id.co.adira.partner.idm.dto;

import org.apache.commons.lang3.time.FastDateFormat;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import id.co.adira.partner.idm.utils.DateUtil;
import id.co.adira.partner.idm.utils.ValueUtil;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@JsonInclude(Include.NON_NULL)
public class AuthResponseDTO<T> {

	@Setter
	@Getter
	@JsonProperty("Status")
	private int status;
	
	@Setter
	@Getter
	@JsonProperty("Message")
	private String message;
	
	@JsonProperty("DateTime")
	private String dateTime = FastDateFormat.getInstance("dd-MM-yyyy HH:mm:ss").format(DateUtil.getCurrentTimestamp());
	
	@Setter
	@Getter
	@JsonProperty("Appversion")
	private String appversion;
	
	@Setter
	@Getter
	@JsonProperty("Data")
	private T data;
	
	public AuthResponseDTO() {	
	}
	
	public AuthResponseDTO(int status, String message, String appversion) {
        this.status = status;
        this.message = message;
        this.appversion = appversion;
    }
	
	public AuthResponseDTO(int status, String message, String appversion, T data) {
		super();

        if(ValueUtil.hasValue(appversion)) { 
        	this.appversion = appversion;
        }
        
        if(ValueUtil.hasValue(message)) { 
        	this.message = message;
        }
        
        this.status = status;
		this.data = data;
	}
	
}
