package id.co.adira.partner.idm.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Positive;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FilterPageAndDataDTO {
    @Positive(message = "Page number must be more than 0 or Positive Number!")
    private Integer page;
    private Integer dataCount;
}
