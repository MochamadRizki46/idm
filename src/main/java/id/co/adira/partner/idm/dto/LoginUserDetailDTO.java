package id.co.adira.partner.idm.dto;

import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonFormat;

import id.co.adira.partner.idm.entity.LoginUserDetail;
import lombok.Getter;
import lombok.Setter;
import javax.validation.constraints.*;

@Setter
@Getter
public class LoginUserDetailDTO {

	private Integer id;
	
	@NotNull
    @Pattern(regexp = "^[_'.@A-Za-z0-9-]*$")
    @Size(min = 1, max = 10)
	private String username;
	
	private String activeStatus;
	
	private boolean isStillLogin;
	
	private boolean isFirstTime;
	
	private Integer loginCount;
	
	@JsonFormat(pattern="yyyy-MM-dd hh:mm:ss")
	private Timestamp lastLoginDate;
	
	public LoginUserDetailDTO() {
    }
	
	public LoginUserDetailDTO(LoginUserDetail loginUserDetail) {
	}
	
}
