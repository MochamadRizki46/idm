package id.co.adira.partner.idm.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MenuAdminHeadResponseDTO {
    private List<MenuAdminDetailResponseDTO> menuList;
    private Integer totalMenu;
}
