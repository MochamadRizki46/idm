package id.co.adira.partner.idm.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ObjectDTO {
    String OBJT_CODE;
    String OBJT_DESC;
}
