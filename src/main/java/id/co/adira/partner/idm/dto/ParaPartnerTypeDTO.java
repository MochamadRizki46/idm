package id.co.adira.partner.idm.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ParaPartnerTypeDTO {
	private String paraPartnerTypeId;
    private String partnerType;
    private String partnerTypeName;

}
