package id.co.adira.partner.idm.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ParaUserDetailAndMenuResDTO {
    String picName;

    String handphoneNo;

    String noWa;

    String birthPlace;

    Date birthDate;

    String userType;

    String partnerId;

    String email;

    String partnerName;

    List<String> eligibleMenus;
}
