package id.co.adira.partner.idm.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class PartnerUrlDTO {
    private int urlId;
    private String serviceName;
    private String baseUrl;
    private String endpointUrl;

}
