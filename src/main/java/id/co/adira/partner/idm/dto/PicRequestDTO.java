package id.co.adira.partner.idm.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PicRequestDTO {

    private String handphoneNo;

    private String picName;

    private String noWa;

    private String birthPlace;

    private String userType;

    private String birthDate;

    private String userId;

    private String partnerName;

    private String partnerCode;

    private List<String> checkedMenuCodes;
}
