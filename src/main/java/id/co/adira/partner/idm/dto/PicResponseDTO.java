package id.co.adira.partner.idm.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PicResponseDTO {
    private Integer id;
    private String userId;
    private String picName;
    private String birthPlace;
    private Date birthDate;
    private String handphoneNo;
    private String noWa;
    private String partnerType;
    private String userType;
    private String partnerCode;
    private String partnerName;
    private Integer isActive;
    private List<CheckedOrUncheckedMenuResponseDTO> checkedMenus;

}
