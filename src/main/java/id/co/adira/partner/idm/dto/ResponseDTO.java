package id.co.adira.partner.idm.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import id.co.adira.partner.idm.utils.DateUtil;
import id.co.adira.partner.idm.utils.ValueUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.time.FastDateFormat;

@JsonInclude(Include.NON_NULL)
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class ResponseDTO<T> {

	@JsonProperty("Status")
	private int status;

	@JsonProperty("Message")
	private String message;
	
	@JsonProperty("DateTime")
	private String dateTime = FastDateFormat.getInstance("dd-MM-yyyy HH:mm:ss").format(DateUtil.getCurrentTimestamp());

	@JsonProperty("Data")
	private T data;

	public ResponseDTO(int status, String message, T data){
		this.status = status;
		this.message = message;
		this.data = data;
	}


}
