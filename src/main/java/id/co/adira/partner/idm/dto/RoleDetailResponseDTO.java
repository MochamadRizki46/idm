package id.co.adira.partner.idm.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RoleDetailResponseDTO {
    private String partnerType;

    private String roleId;

    private String roleName;

    private Integer active;

    private List<CheckedOrUncheckedMenuResponseDTO> checkedMenus;

}
