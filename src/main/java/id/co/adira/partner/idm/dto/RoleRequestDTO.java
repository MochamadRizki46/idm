package id.co.adira.partner.idm.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Size;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RoleRequestDTO {
	@Size(min = 3, max = 3, message = "Partner Type must be 3 digits")
    private String partnerType;
    @Size(min = 3, max = 3, message = "Role id must be 3 digits")
    private String roleId;
    private String roleName;
    private List<String> checkedMenuCodes;
}
