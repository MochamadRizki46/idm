package id.co.adira.partner.idm.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

public class StandardResponseDTO {

	@Setter
	@Getter
	@JsonProperty("Status")
	private int status;
	
	@Setter
	@Getter
	@JsonProperty("Message")
	private String message;
	
	public StandardResponseDTO() {	
	}
	
	public StandardResponseDTO(int status, String message) {
        this.status = status;
        this.message = message;
    }
}
