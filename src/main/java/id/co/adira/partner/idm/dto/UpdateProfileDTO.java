package id.co.adira.partner.idm.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author 10999943
 *
 */
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class UpdateProfileDTO {

	@ApiModelProperty(position = 1)
	private String userId;
	@ApiModelProperty(position = 2)
	private String userName;
	@ApiModelProperty(position = 3)
	private String email;
	@ApiModelProperty(position = 4)
	private String phoneNumber;
	@ApiModelProperty(position = 5)
	private String waNumber;

}