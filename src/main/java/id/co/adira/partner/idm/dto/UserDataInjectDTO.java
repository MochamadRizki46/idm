/**
 * 
 */
package id.co.adira.partner.idm.dto;



import lombok.Setter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * @author Vionza
 *
 */
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class UserDataInjectDTO {

	private String branchCode;
	private String dlc;
	private String dealerName;
	private String prodMatrix;
	private String brandName;
	private String salesFullname;
	private String phoneNumber;
	private String email;
	private String jabatan;
	private String channel;
	
}
