package id.co.adira.partner.idm.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserDetailDTO implements Serializable {
    private static final long serialVersionUID = 1L;


    @JsonIgnore
    private Integer id;


    private String userType;


    private String userId;


    private String roleId;


    private String placeOfBirth;


    private Date birthday;


    private String email;


    private String handphone;


    private String noWa;


    private String partnerCode;


    private String partnerName;


    private String createBy;


    private Date createDate;


    private String modifyBy;


    private Date modifyDate;


    private Integer isactive;


    private Integer deleted;


    private String portfolio;


    private String brand;


    private String username;
}
