package id.co.adira.partner.idm.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserDetailDTOV3 implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonIgnore
    Integer id;

    String userType;

    String userId;

    String roleId;

    String placeOfBirth;

    Date birthday;

    String email;

    String handphone;

    String noWa;

    String partnerCode;

    String partnerName;

    String createBy;

    Date createDate;

    String modifyBy;

    Date modifyDate;

    Integer isactive;

    Integer deleted;

    List<PortfolioDTO> portfolios;

    String brand;

    String username;

    String ownerDealer;

}
