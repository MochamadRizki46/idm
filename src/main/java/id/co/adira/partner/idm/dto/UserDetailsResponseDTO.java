package id.co.adira.partner.idm.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserDetailsResponseDTO {
    private Integer id;

    private String userId;

    private String username;

    private String email;

    private String handphoneNo;

    private String userType;

    private String partnerId;

    private String partnerName;

    private Integer isActive;

}
