package id.co.adira.partner.idm.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class UserLoginRequest {
	@ApiModelProperty(position = 1)
	String login;
	@ApiModelProperty(position = 2)
	String password;

}
