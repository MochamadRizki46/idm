package id.co.adira.partner.idm.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;


@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class VersionDTO {
    private String latestApkVersion;
    private String description;
    private List<PartnerUrlDTO> partnerUrls;
}
