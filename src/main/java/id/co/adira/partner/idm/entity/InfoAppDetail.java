package id.co.adira.partner.idm.entity;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "tbl_info_app_detail")
@Data
public class InfoAppDetail {

    @Id
    @Column(name = "url_detail_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "app_detail_sequence")
    @SequenceGenerator(name = "app_detail_sequence", sequenceName = "app_detail_seq", allocationSize = 1)
    private int urlDetailId;

    @Column(name = "apk_version")
    private String apkVersion;

    @Column(name = "description")
    private String description;

    @Column(name = "created_date")
    @CreationTimestamp
    private Date createdDate;

    @Column(name = "modified_date")
    @UpdateTimestamp
    private Date modifiedDate;

}
