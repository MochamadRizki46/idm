package id.co.adira.partner.idm.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "login_user_detail")
@Data
@EqualsAndHashCode(callSuper=false)
public class LoginUserDetail extends AbstractAuditingEntity implements Serializable {
	
	private static final long serialVersionUID = 6536852096616607469L;
	
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "lud_seq_gen")
	@SequenceGenerator(name = "lud_seq_gen", sequenceName = "lud_seq_gen", allocationSize = 1)
	private Integer id;
	
	@Column(name = "username")
	private String username;
	
	@Column(name = "active_status")
	private String activeStatus;
	
	@Column(name = "is_still_login")
	private boolean isStillLogin;

	@Column(name = "is_first_time")
	private boolean isFirstTime;
	
	@Column(name = "login_count")
	private Integer loginCount;
	
	@Column(name = "last_login_date")
	private Timestamp lastLoginDate;

}
