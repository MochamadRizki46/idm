package id.co.adira.partner.idm.entity;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "para_role_menu")
@Data
public class ParaRoleMenu {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "para_role_menu_id")
    String paraRoleMenuId;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_date")
    Date createdDate;

    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "modified_date")
    Date modifiedDate;

    @Column(name = "created_by")
    String createdBy;

    @Column(name = "modified_by")
    String modifiedBy;

    @Column(name = "active")
    Integer active;

    @ManyToOne
    @JoinColumn(name = "para_role_id")
    RoleDetail roleDetail;

    @ManyToOne
    @JoinColumn(name = "para_menu_id")
    ParaMenuAdmin paraMenuAdmin;
}
