package id.co.adira.partner.idm.entity;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name=  "para_user_detail_menu")
@Data
public class ParaUserDetailMenu {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "para_user_detail_menu_id")
    private String paraUserDetailMenuId;

    @ManyToOne
    @JoinColumn(name = "para_user_detail_id")
    UserDetail userDetail;

    @ManyToOne
    @JoinColumn(name = "para_menu_admin_id")
    ParaMenuAdmin paraMenuAdmin;

    @Column(name = "active")
    Integer active;

    @Column(name = "created_date")
    @Temporal(TemporalType.TIMESTAMP)
    @CreationTimestamp
    private Date createdDate;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "modified_date")
    @Temporal(TemporalType.TIMESTAMP)
    @UpdateTimestamp
    private Date modifiedDate;

    @Column(name = "modified_by")
    private String modifiedBy;
}
