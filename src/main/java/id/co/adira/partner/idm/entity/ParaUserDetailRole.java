package id.co.adira.partner.idm.entity;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "para_user_detail_role")
@Data
public class ParaUserDetailRole {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "para_user_detail_role_id")
    String paraUserDetailRoleId;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_date")
    Date createdDate;

    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "modified_date")
    Date modifiedDate;

    @Column(name = "created_by")
    String createdBy;

    @Column(name = "modified_by")
    String modifiedBy;

    @OneToOne
    @JoinColumn(name = "para_user_detail_id")
    UserDetail userDetail;

    @OneToOne
    @JoinColumn(name = "para_user_role_id")
    RoleDetail roleDetail;

}
