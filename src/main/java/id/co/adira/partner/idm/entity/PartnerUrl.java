package id.co.adira.partner.idm.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_list_url_partner")
@Data
public class PartnerUrl {

    @Id
    @Column(name = "url_id")
    private int urlId;

    @Column(name = "service_name")
    private String serviceName;

    @Column(name = "base_url")
    private String baseUrl;


    @Column(name = "endpoint_url")
    private String endpointUrl;

}
