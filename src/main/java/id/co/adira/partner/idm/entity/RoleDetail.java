/**
 * 
 */
package id.co.adira.partner.idm.entity;

import java.util.Date;

import javax.persistence.*;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

@Data
@Table(name = "para_role")
@Entity
public class RoleDetail {

	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
	@Column(name = "para_role_id")
	private String paraRoleId;

	@Column(name = "role_id")
	private String roleId;

	@Column(name = "role_code")
	private String roleCode;
	
	@Column(name = "role_name")
	private String roleName;
	
	@Column(name = "isactive")
	private Integer isActive;
	
	@Column(name = "create_by")
	private String createBy;
	
	@Column(name = "createdate")
	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;
	
	@Column(name = "modify_by")
	private String modifyBy;
	
	@Column(name = "modifydate")
	@UpdateTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	private Date modifyDate;

	@Column(name = "deleted")
	private Integer deleted;

}
