/**
 * 
 */
package id.co.adira.partner.idm.entity;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import lombok.Data;

/**
 * @author Vionza
 *
 */
@Table(name = "tbl_data_user_inject_history")
@Entity
@Data
public class UserDataInjectHist {

	@Column(name = "branch_code")
	private String branchCode;
	@Column(name = "dlc")
	private String dlc;
	@Column(name = "dealer_name")
	private String dealerName;
	@Column(name = "product_matrix")
	private String prodMatrix;
	@Column(name = "brand")
	private String brandName;
	@Column(name = "nama_lengkap_sales")
	private String salesFullname;
	@Column(name = "no_hp")
	private String phoneNumber;
	
	@Id
	@Column(name = "email")
	private String email;
	
	@Column(name = "jabatan")
	private String jabatan;
	@Column(name = "channel")
	private String channel;
	@Column(name = "tanggal_upload")
	@UpdateTimestamp
	private Date uploadDate;
}
