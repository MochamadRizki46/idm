/**
 * 
 */
package id.co.adira.partner.idm.entity;

import java.util.Date;

import javax.persistence.*;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Table(name = "para_user_detail")
@Data
public class UserDetail {

	@Id
	@Column(name = "id")
	private Integer id;

	@Column(name = "usertype")
	private String userType;

	@Column(name ="userid")
	private String userId;
	
	@Column(name ="roleid")
	private String roleId;

	@Column(name ="placeofbirth")
	private String placeOfBirth;

	@Column(name ="birthday")
	private Date birthday;

	@Column(name ="email")
	private String email;
	
	@Column(name ="handphone")
	private String handphone;
	
	@Column(name ="wa")
	private String noWa;
	
	@Column(name ="partner_code")
	private String partnerCode;
	
	@Column(name ="partner_name")
	private String partnerName;
	
	@Column(name ="create_by")
	private String createBy;
	
	@Column(name ="createdate")
	@Temporal(TemporalType.TIMESTAMP)
	@CreationTimestamp
	private Date createDate;
	
	@Column(name ="modify_by")
	private String modifyBy;
	
	@Column(name ="modifydate")
	@Temporal(TemporalType.TIMESTAMP)
	@UpdateTimestamp
	private Date modifyDate;
	
	@Column(name ="isactive")
	private Integer isactive;
	
	@Column(name ="deleted")
	private Integer deleted;
	
	@Column(name ="portfolio")
	private String portfolio;
	
	@Column(name ="brand")
	private String brand;
	
	@Column(name ="username")
	private String username;
	
	
}
