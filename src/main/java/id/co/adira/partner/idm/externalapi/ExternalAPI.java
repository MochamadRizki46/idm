package id.co.adira.partner.idm.externalapi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class ExternalAPI {

	@Autowired
	private RestTemplate template;  
	
	/**
	 * @param url
	 * @return external api get call
	 */
	public Object getApiCall(String url) {
		return template.getForEntity(url, Object.class);
	}
}
