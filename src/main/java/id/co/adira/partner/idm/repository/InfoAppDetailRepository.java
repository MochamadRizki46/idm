package id.co.adira.partner.idm.repository;

import id.co.adira.partner.idm.entity.InfoAppDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InfoAppDetailRepository extends JpaRepository<InfoAppDetail, Integer> {
    @Query("SELECT a FROM InfoAppDetail a ORDER BY a.createdDate DESC")
    List<InfoAppDetail> findAllInfoAppDetail();
}
