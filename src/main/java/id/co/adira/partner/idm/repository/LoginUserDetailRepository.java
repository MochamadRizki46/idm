package id.co.adira.partner.idm.repository;

import java.util.List;
import org.springframework.stereotype.Repository;

import id.co.adira.partner.idm.entity.LoginUserDetail;

import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface LoginUserDetailRepository extends JpaRepository<LoginUserDetail, Integer> {

	List<LoginUserDetail> findByUsername(String username);
	
	LoginUserDetail findByid(Integer id);
	
}
