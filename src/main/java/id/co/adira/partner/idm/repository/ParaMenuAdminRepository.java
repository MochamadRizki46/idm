package id.co.adira.partner.idm.repository;

import id.co.adira.partner.idm.entity.ParaMenuAdmin;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface ParaMenuAdminRepository extends JpaRepository<ParaMenuAdmin, String> {
    @Query("select a from ParaMenuAdmin a where a.active = 0")
    List<ParaMenuAdmin> findAllActiveMenu(Pageable pageable);

    @Query("select a from ParaMenuAdmin a where a.deleted = 0 order by a.paraMenuCode asc")
    List<ParaMenuAdmin> findAllMenuWithoutConcerningIsActiveWithPagination(Pageable pageable);

    @Query("select a from ParaMenuAdmin a order by a.paraMenuCode asc")
    List<ParaMenuAdmin> findAllMenuWithoutPagination();

    @Query("select count(a.paraMenuAdminId) from ParaMenuAdmin a")
    Integer countDataParaMenuAdmin();

    @Query("select a from ParaMenuAdmin a order by a.paraMenuCode desc")
    List<ParaMenuAdmin> findAllMenuOrderByMenuCodeDesc();

    @Query("select a from ParaMenuAdmin a where a.paraMenuCode = :menuCode")
    ParaMenuAdmin findParaMenuAdminBasedOnMenuCode(String menuCode);

    @Query("select a from ParaMenuAdmin a where a.paraMenuAdminId = :menuId and a.deleted = 0")
    ParaMenuAdmin findParaMenuAdminBasedOnMenuId(String menuId);

    @Query("select a from ParaMenuAdmin a where a.paraMenuCode = :menuCode")
    ParaMenuAdmin findMenuByMenuCode(String menuCode);

    @Query("update ParaMenuAdmin a set a.active = 1 where a.paraMenuAdminId = :paraMenuAdminId")
    @Modifying
    void nonActivingMenuByParaMenuAdminId(String paraMenuAdminId);

    @Query("update ParaMenuAdmin a set a.active = 0 where a.paraMenuAdminId = :paraMenuAdminId")
    @Modifying
    void activingMenuByParaMenuAdminId(String paraMenuAdminId);

    @Query("update ParaMenuAdmin a set a.deleted = 1 where a.paraMenuAdminId = :paraMenuAdminId")
    @Modifying
    void setStatusDeletedByMenuId(String paraMenuAdminId);
}
