package id.co.adira.partner.idm.repository;

import id.co.adira.partner.idm.dto.PortfolioDTO;
import id.co.adira.partner.idm.entity.ParaObject;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface ParaObjectRepository extends JpaRepository<ParaObject, String> {

    @Query("select distinct new id.co.adira.partner.idm.dto.PortfolioDTO(po.paraObjectId, po.paraObjectName) " +
            "from ErpBpIndustry ebi join ErpBpRole ebr on " +
            "ebi.erpBpBpartner = ebr.erpBpBpartner " +
            "and ebr.erpBpRoleCategory = 'ZP0101' " +
            "join ParaObject po on " +
            "po.paraObjectId = ebi.erpBpkeySystem " +
            "and ebr.erpBpPartnerExternal = :dealerCode " +
            "order by 1"
    )
    List<PortfolioDTO> getPortfolio(String dealerCode);

}
