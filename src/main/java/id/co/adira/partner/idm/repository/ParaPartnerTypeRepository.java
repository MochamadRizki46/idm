package id.co.adira.partner.idm.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import id.co.adira.partner.idm.dto.ParaPartnerTypeDTO;
import id.co.adira.partner.idm.entity.ParaPartnerType;

public interface ParaPartnerTypeRepository extends JpaRepository<ParaPartnerType, String>{
	@Query("select new id.co.adira.partner.idm.dto.ParaPartnerTypeDTO(a.paraPartnerTypeId, a.partnerType, " + 
			"a.partnerTypeName) from ParaPartnerType a where a.active = 0")
    List<ParaPartnerTypeDTO> findAllActivePartnerType();

}
