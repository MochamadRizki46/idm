package id.co.adira.partner.idm.repository;

import id.co.adira.partner.idm.entity.ParaRoleMenu;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface ParaRoleMenuRepository extends JpaRepository<ParaRoleMenu, String> {
    @Query("select a from ParaRoleMenu a where a.roleDetail.paraRoleId = :paraRoleId and a.paraMenuAdmin.paraMenuAdminId = :menuId")
    ParaRoleMenu findParaRoleMenuBasedOnParaRoleIdAndMenuAdminId(String paraRoleId, String menuId);

    @Query("select a from ParaRoleMenu a where a.roleDetail.paraRoleId = :paraRoleId and active = 0 order by a.createdDate asc")
    List<ParaRoleMenu> findParaRoleMenuByParaRoleId(String paraRoleId);

    @Modifying
    @Query("delete from ParaRoleMenu a where a.roleDetail.paraRoleId = :id")
    void deleteMenuByRoleId(String id);

}
