package id.co.adira.partner.idm.repository;

import id.co.adira.partner.idm.entity.ParaUserDetailMenu;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface ParaUserDetailMenuRepository extends JpaRepository <ParaUserDetailMenu, String> {
    @Query("select a from ParaUserDetailMenu a where a.userDetail.id = :id and a.paraMenuAdmin.active = 0 order by a.paraMenuAdmin.paraMenuCode asc")
    List<ParaUserDetailMenu> findAllUserDetailMenuById(Integer id);

    @Query("select a from ParaUserDetailMenu a where a.userDetail.id = :id and a.paraMenuAdmin.paraMenuAdminId = :menuId")
    ParaUserDetailMenu findAllUserDetailMenuByUserIdAndMenuId(Integer id, String menuId);

    @Query("select a from ParaUserDetailMenu a where a.userDetail.id = :id and a.active = 0")
    List<ParaUserDetailMenu> findAllEligibleMenuForUserByUserId(Integer id);

    @Query("select a from ParaUserDetailMenu a where a.userDetail.id = :id and a.active = 1")
    List<ParaUserDetailMenu> findAllNonEligibleMenuForUserByUserId(Integer id);
    
    @Modifying
    @Query("delete from ParaUserDetailMenu a where a.userDetail.id = :id")
    void deleteMenuByUserId(Integer id);
}
