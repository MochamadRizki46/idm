package id.co.adira.partner.idm.repository;

import id.co.adira.partner.idm.entity.ParaUserDetailRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface ParaUserDetailRoleRepository extends JpaRepository<ParaUserDetailRole, String> {
    @Query("select a from ParaUserDetailRole a where a.userDetail.id = :id and a.roleDetail.roleId = :userType")
    ParaUserDetailRole findParaUserDetailRoleByIdUserAndRoleId(Integer id, String userType);

    @Query("select a from ParaUserDetailRole a where a.roleDetail.paraRoleId = :roleId")
    List<ParaUserDetailRole> findParaUserDetailRoleByRoleId(String roleId);
}
