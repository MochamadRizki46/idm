package id.co.adira.partner.idm.repository;

import id.co.adira.partner.idm.entity.PartnerUrl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PartnerUrlRepository extends JpaRepository<PartnerUrl, Integer> {
    @Query("SELECT a FROM PartnerUrl a ORDER BY a.urlId ASC")
    List<PartnerUrl> findAllSortedById();
}
