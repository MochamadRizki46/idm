package id.co.adira.partner.idm.repository;

import id.co.adira.partner.idm.entity.RoleDetail;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface RoleRepository extends JpaRepository<RoleDetail, String> {
    @Query("select count(a.paraRoleId) from RoleDetail a")
    Integer countRoleData();

    @Query("select a from RoleDetail a where a.roleId = :roleCode")
    RoleDetail findUserRoleBasedOnRoleCode(String roleCode);

    @Query("select a from RoleDetail a where a.roleId = :roleId and a.deleted = 0")
    RoleDetail findUndeletedUserRoleBasedOnRoleId(String roleId);

    @Query("select a from RoleDetail a where a.deleted = 0 order by a.roleCode asc")
    List<RoleDetail> findAllUserRole(Pageable pageable);

    @Query("update RoleDetail a set a.isActive = 0 where a.roleId = :roleId")
    @Modifying
    void activateRole(String roleId);

    @Query("update RoleDetail a set a.isActive = 1 where a.roleId = :roleId")
    @Modifying
    void nonActivateRole(String roleId);

    @Query("update RoleDetail a set a.deleted = 1 where a.roleId = :roleId")
    @Modifying
    void setToDeletedByRoleCode(String roleId);

    @Query("select a from RoleDetail a order by a.roleCode desc")
    List<RoleDetail> findAllRoleDetailsOrderByRoleCodeDesc();
    
    @Query("select a from RoleDetail a where a.deleted = 0 and a.roleCode = :partnerType order by a.roleCode asc")
    List<RoleDetail> findRoleByPartnerType(String partnerType);
}
