package id.co.adira.partner.idm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import id.co.adira.partner.idm.entity.UserDataInjectHist;

@Repository
public interface UserDataInjectHistRepository extends JpaRepository<UserDataInjectHist, String> {

}
