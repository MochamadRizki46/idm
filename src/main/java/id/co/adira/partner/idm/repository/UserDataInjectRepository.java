/**
 * 
 */
package id.co.adira.partner.idm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import id.co.adira.partner.idm.entity.UserDataInject;

/**
 * @author Vionza
 *
 */
@Repository
public interface UserDataInjectRepository extends JpaRepository<UserDataInject, String> {

@Transactional
@Modifying
@Query(value = "insert into para_user_detail "
		+ "select nextval('user_id_seq') id,"
		+ "case"
		+ "	when upper(trim(channel)) = 'NDS_AXI' then 'AXI'"
		+ "	   when upper(trim(channel)) = 'KEDAY' then 'KDY'"
		+ "	   when upper(trim(product_matrix)) = 'DURABLE' and upper(trim(jabatan)) in ('STAFF','SALES') then 'SLM'"
		+ "	   when upper(trim(product_matrix)) = 'DURABLE' and upper(trim(jabatan)) = 'SUPERVISOR' then 'BMM'"
		+ "	   when upper(trim(product_matrix)) = 'DURABLE' and upper(trim(jabatan)) = 'OWNER' then 'OWM'"
		+ "	   when upper(trim(jabatan)) in ('STAFF','SALES') then 'SLD'"
		+ "	   when upper(trim(jabatan)) = 'SUPERVISOR' then 'BMD'"
		+ "	   when upper(trim(jabatan)) = 'OWNER' then 'OWD'"
		+ "	   when upper(trim(jabatan)) = 'SUPER ADMIN' then 'ADM'"
		+ "	   else '-' end usertype,"
		+ "	   lower(trim(email)) userid,"
		+ "	   case "
		+ "	   when upper(trim(channel)) = 'NDS_AXI' then 'AXI'"
		+ "	   when upper(trim(channel)) = 'KEDAY' then 'KDY'"
		+ "	   when upper(trim(product_matrix)) = 'DURABLE' and upper(trim(jabatan)) in ('STAFF','SALES') then 'SLM'"
		+ "	   when upper(trim(product_matrix)) = 'DURABLE' and upper(trim(jabatan)) = 'SUPERVISOR' then 'BMM'"
		+ "	   when upper(trim(product_matrix)) = 'DURABLE' and upper(trim(jabatan)) = 'OWNER' then 'OWM'"
		+ "	   when upper(trim(jabatan)) in ('STAFF','SALES') then 'SLD'"
		+ "	   when upper(trim(jabatan)) = 'SUPERVISOR' then 'BMD'"
		+ "	   when upper(trim(jabatan)) = 'OWNER' then 'OWD'"
		+ "	   when upper(trim(jabatan)) = 'SUPER ADMIN' then 'ADM'"
		+ "	   else '-' end roleid,"
		+ "	   null placeofbirth,"
		+ "	   null birthday,"
		+ "	   lower(trim(email)) email,"
		+ "	   trim(no_hp) handphone,"
		+ "	   trim(no_hp) wa,"
		+ "	   trim(dlc) partner_code,"
		+ "	   trim(dealer_name) partner_name,"
		+ "	   'SYSTEM' create_by,"
		+ "	   now() createdate,"
		+ "	   'SYSTEM' modify_by,"
		+ "	   now() modifydate,"
		+ "	   0 isactive,"
		+ "	   0 deleted, "
		+ "	   case "
		+ "	   when upper(trim(channel)) = 'NDS_AXI' then 'AXI'"
		+ "	   when upper(trim(channel)) = 'KEDAY' then 'KDY'"
		+ "	   when upper(trim(product_matrix)) like 'MOTOR BARU%' then 'NMCY'"
		+ "	   when upper(trim(product_matrix)) like 'MOTOR BEKAS%' then 'UMCY'"
		+ "	   when upper(trim(product_matrix)) like 'MOBIL BARU%' then 'NCAR'"
		+ "	   when upper(trim(product_matrix)) like 'MOBIL BEKAS%' then 'UCAR'"
		+ "	   when upper(trim(product_matrix)) = 'DURABLE' and upper(trim(jabatan)) in ('STAFF','SALES') then 'SLM'"
		+ "	   when upper(trim(product_matrix)) = 'DURABLE' and upper(trim(jabatan)) = 'SUPERVISOR' then 'BMM'"
		+ "	   when upper(trim(product_matrix)) = 'DURABLE' and upper(trim(jabatan)) = 'OWNER' then 'OWM'"
		+ "	   else '-'"
		+ "	   end portofolio,"
		+ "	   case when upper(trim(brand)) = 'MULTIBRAND' then 'ALL' else upper(trim(brand)) end brand,"
		+ "	   trim(nama_lengkap_sales) username"
		+ "	   from public.tbl_data_user_inject tdu "
		+ "where not exists (select 1 from para_user_detail pud "
		+ "where upper(trim(tdu.email)) = upper(pud.userid) "
		+ "and pud.deleted = 0)" , nativeQuery = true)
void mappingIntoMainTableUser();
}
