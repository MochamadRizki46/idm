/**
 * 
 */
package id.co.adira.partner.idm.repository;

import id.co.adira.partner.idm.entity.UserDetail;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * @author 10999943
 *
 */
@Repository
@Transactional
public interface UserRepository extends JpaRepository<UserDetail, Integer> {

	@Query("select o from UserDetail o where UPPER(o.userId) = UPPER(:userId) AND o.isactive = 0 AND o.deleted = 0")
	UserDetail findUserByUserId(@Param("userId") String userId);

	@Query("select o from UserDetail o where UPPER(o.userId) = UPPER(:userId)")
	UserDetail findUserByIdWithoutConcerningActive(@Param("userId") String userId);

	@Query("select a from UserDetail a where a.userId = :userId and a.deleted = 0")
	UserDetail findUserByUserIdAndIdWithoutConcerningActive(@Param("userId") String userId);

	@Query("select a from UserDetail a where a.partnerCode = :partnerCode AND a.roleId = 'OWD' AND a.isactive = 0")
	UserDetail findUserOwnerDealerByPartnerCode(String partnerCode);

	@Query("select a from UserDetail a where a.partnerCode = :partnerCode and a.roleId = 'ADM' and a.isactive = 0")
	UserDetail findUserAdminDealerByPartnerCode(String partnerCode);

	@Query("select a from UserDetail a where a.deleted = 0 order by a.id asc")
	List<UserDetail> findAllActiveUserByUsingPageable(Pageable pageable);

	@Query("select count(a.id) from UserDetail a")
	Integer countDataUser();

	@Query("select count(a.id) from UserDetail a where a.deleted = 0")
	Integer countDataUserExceptDeletedUsers();

	@Query("update UserDetail a set a.isactive = 1 where a.userId = :userId")
	@Modifying
	void nonActivingUserBasedOnUserId(String userId);

	@Query("update UserDetail a set a.isactive = 0 where a.userId = :userId")
	@Modifying
	void activingUserBasedOnUserId(String userId);

	@Query("select a from UserDetail a where lower(a.username) like %:username% and a.deleted = 0")
	List<UserDetail> findUserByUsername(String username);

	@Query("select a from UserDetail a where a.id = :id")
	UserDetail findUserById(Integer id);

	@Query("update UserDetail a set a.deleted = 1 where a.userId = :userId")
	@Modifying
	void updateIsDeletedToOne(String userId);

	@Query("select a from UserDetail a order by a.id desc")
	List<UserDetail> findLatestIdUserSequence();
}
