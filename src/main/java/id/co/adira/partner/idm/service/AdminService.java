package id.co.adira.partner.idm.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import id.co.adira.partner.idm.dto.*;
import id.co.adira.partner.idm.entity.ParaMenuAdmin;
import id.co.adira.partner.idm.entity.ParaUserDetailMenu;
import id.co.adira.partner.idm.entity.RoleDetail;
import id.co.adira.partner.idm.entity.UserDataInject;
import id.co.adira.partner.idm.entity.UserDetail;
import id.co.adira.partner.idm.entity.*;
import id.co.adira.partner.idm.repository.*;
import id.co.adira.partner.idm.utils.FormatUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static id.co.adira.partner.idm.constant.AppEnum.*;
import static id.co.adira.partner.idm.constant.DateFormatEnum.DAY_MONTH_YEAR_FORMAT;
import static id.co.adira.partner.idm.constant.LoggerEnum.*;
import static id.co.adira.partner.idm.constant.StatusEnum.*;

@Service
public class AdminService {

	private static final Logger LOGGER = LogManager.getLogger(AdminService.class);

	private final ParaUserDetailMenuRepository paraUserDetailMenuRepository;
	private final ParaMenuAdminRepository paraMenuAdminRepository;
	private final UserRepository userRepository;
	private final RoleRepository roleRepository;
	private final UserDataInjectRepository userDataInjectRepository;
	private final ParaRoleMenuRepository paraRoleMenuRepository;
	private final UserDataInjectHistRepository userDataInjectHistRepository;
	private final ParaPartnerTypeRepository paraPartnerTypeRepository;
	private final FormatUtil formatUtil;

	ObjectMapper objectMapper = new ObjectMapper();
	ModelMapper modelMapper = new ModelMapper();
	SimpleDateFormat ddMMYyyyFormat = new SimpleDateFormat(DAY_MONTH_YEAR_FORMAT.getMessage());

	@Autowired
	public AdminService(ParaUserDetailMenuRepository paraUserDetailMenuRepository,
			ParaMenuAdminRepository paraMenuAdminRepository, UserRepository userRepository,
			RoleRepository roleRepository, FormatUtil formatUtil, UserDataInjectRepository userDataInjectRepository,
			ParaRoleMenuRepository paraRoleMenuRepository, ParaUserDetailRoleRepository paraUserDetailRoleRepository,
			ParaPartnerTypeRepository paraPartnerTypeRepository, UserDataInjectHistRepository userDataInjectHistRepository) {

		this.paraUserDetailMenuRepository = paraUserDetailMenuRepository;
		this.paraMenuAdminRepository = paraMenuAdminRepository;
		this.userRepository = userRepository;
		this.roleRepository = roleRepository;
		this.paraRoleMenuRepository = paraRoleMenuRepository;
		this.userDataInjectHistRepository = userDataInjectHistRepository;
		this.formatUtil = formatUtil;
		this.userDataInjectRepository = userDataInjectRepository;
		this.paraPartnerTypeRepository = paraPartnerTypeRepository;
	}

	public BaseResponse insertOrUpdatePic(PicRequestDTO picRequestDTO) {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setHttpStatus(HttpStatus.OK);
		baseResponse.setStatus(0);
		baseResponse.setMessage(UPDATE_SUCCESS.getMessage());

		try {
			System.out.println("masuk");
			UserDetail currentUserDetail = userRepository.findUserByUserId(picRequestDTO.getUserId());
			mapAnExistingUserDetail(currentUserDetail, picRequestDTO, baseResponse);
			paraUserDetailMenuRepository.deleteMenuByUserId(currentUserDetail.getId());
			insertMenuEligibleForPic(currentUserDetail, picRequestDTO.getCheckedMenuCodes());
			baseResponse.setData(currentUserDetail.getUserId());
			LOGGER.info(SUCCESS_SAVING_DATA_TO_DB.getMessage());
		} catch (Exception e) {
			insertErrorResponse(baseResponse, ERROR_WHILE_SAVING_PIC_TO_DB.getMessage(), e);
		}
		return baseResponse;
	}

	private void mapAnExistingUserDetail(UserDetail currentUserDetail, PicRequestDTO picRequestDTO,
			BaseResponse baseResponse) throws ParseException {
		System.out.println("masuk1");
		UserDetail existingUser = insertPicToAnExistingData(picRequestDTO, currentUserDetail);
//		updateAllMenuToInactive(currentUserDetail);

		baseResponse.setData(existingUser.getUserId());
	}

	private void mapNewUserDetail(PicRequestDTO picRequestDTO, BaseResponse baseResponse)
			throws ParseException, JsonProcessingException {
		UserDetail newUserDetail = insertNewPicIntoTable(picRequestDTO);
		insertMenuEligibleForPic(newUserDetail, picRequestDTO.getCheckedMenuCodes());

		baseResponse.setData(insertDataPicToResponse(newUserDetail));
	}


	private Object insertDataPicToResponse(UserDetail newUserDetail) {
		return new ParaUserDetailAndMenuResDTO(newUserDetail.getUserId(), newUserDetail.getHandphone(),
				newUserDetail.getNoWa(), newUserDetail.getPlaceOfBirth(), newUserDetail.getBirthday(),
				newUserDetail.getUserType(), newUserDetail.getPartnerCode(), newUserDetail.getEmail(),
				newUserDetail.getPartnerName(), insertEligibleMenuToResponse(newUserDetail));
	}

	private List<String> insertEligibleMenuToResponse(UserDetail newUserDetail) {
		List<String> menus = new ArrayList<>();
		List<ParaUserDetailMenu> userDetailMenus = paraUserDetailMenuRepository
				.findAllEligibleMenuForUserByUserId(newUserDetail.getId());

		if (userDetailMenus.isEmpty())
			return new ArrayList<>();
		else {
			for (ParaUserDetailMenu paraUserDetailMenu : userDetailMenus) {
				ParaMenuAdmin paraMenuAdmin = paraMenuAdminRepository
						.findParaMenuAdminBasedOnMenuId(paraUserDetailMenu.getParaMenuAdmin().getParaMenuAdminId());
				menus.add(paraMenuAdmin.getParaMenuAdminName());
			}
			return menus;
		}
	}

	private void insertMenuEligibleForPic(UserDetail userDetail, List<String> checkedMenuCodes) {

		System.out.println("masuk2");
		for (String checkedMenuCode : checkedMenuCodes) {
			ParaMenuAdmin paraMenuAdmin = paraMenuAdminRepository.findMenuByMenuCode(checkedMenuCode);
			ParaUserDetailMenu paraUserDetailMenu = paraUserDetailMenuRepository
					.findAllUserDetailMenuByUserIdAndMenuId(userDetail.getId(), paraMenuAdmin.getParaMenuAdminId());

			if (paraUserDetailMenu == null)
				insertNewUserDetailMenuForCheckedMenu(userDetail, paraMenuAdmin, new ParaUserDetailMenu());

		}
	}


	private void insertNewUserDetailMenuForCheckedMenu(UserDetail userDetail, ParaMenuAdmin paraMenuAdmin,
			ParaUserDetailMenu paraUserDetailMenu) {
		paraUserDetailMenu.setUserDetail(userDetail);
		paraUserDetailMenu.setParaMenuAdmin(paraMenuAdmin);
		paraUserDetailMenu.setActive(0);
		paraUserDetailMenu.setCreatedBy(ADMIN.getMessage());
		paraUserDetailMenu.setModifiedBy(ADMIN.getMessage());

		paraUserDetailMenuRepository.save(paraUserDetailMenu);
	}

	private void insertErrorResponse(BaseResponse baseResponse, String message, Exception e) {
		baseResponse.setHttpStatus(HttpStatus.INTERNAL_SERVER_ERROR);
		baseResponse.setStatus(1);
		baseResponse.setMessage(THERE_IS_SOMETHING_ERROR_IN_OUR_SERVER.getMessage());
		baseResponse.setData(null);

		LOGGER.error(message, e.getMessage());
	}

	private UserDetail insertPicToAnExistingData(PicRequestDTO picRequestDTO, UserDetail currentUserDetail)
			throws ParseException {
		currentUserDetail.setUserId(picRequestDTO.getUserId());
		currentUserDetail.setHandphone(picRequestDTO.getHandphoneNo());
		currentUserDetail.setNoWa(picRequestDTO.getNoWa());
		currentUserDetail.setEmail(picRequestDTO.getUserId());
		currentUserDetail.setUserType(picRequestDTO.getUserType());
		currentUserDetail.setRoleId(picRequestDTO.getUserType());
		currentUserDetail.setIsactive(0);
		currentUserDetail.setUsername(picRequestDTO.getPicName());
		currentUserDetail.setModifyBy(ADMIN.getMessage());
		currentUserDetail.setCreateBy(ADMIN.getMessage());
		currentUserDetail.setBirthday(ddMMYyyyFormat.parse(picRequestDTO.getBirthDate()));
		currentUserDetail.setPlaceOfBirth(picRequestDTO.getBirthPlace());
		currentUserDetail.setPartnerCode(picRequestDTO.getPartnerCode());
		currentUserDetail.setPartnerName(picRequestDTO.getPartnerName());

		userRepository.save(currentUserDetail);
		return currentUserDetail;
	}

	private UserDetail insertNewPicIntoTable(PicRequestDTO picRequestDTO)
			throws JsonProcessingException, ParseException {
		UserDetail newUserDetail = new UserDetail();
		newUserDetail.setId(userRepository.findLatestIdUserSequence().get(0).getId() + 1);
		newUserDetail.setEmail(picRequestDTO.getUserId());
		newUserDetail.setHandphone(picRequestDTO.getHandphoneNo());
		newUserDetail.setNoWa(picRequestDTO.getNoWa());
		newUserDetail.setUserType(picRequestDTO.getUserType());
		newUserDetail.setIsactive(0);
		newUserDetail.setUsername(picRequestDTO.getPicName());
		newUserDetail.setModifyBy(ADMIN.getMessage());
		newUserDetail.setCreateBy(ADMIN.getMessage());
		newUserDetail.setUserId(picRequestDTO.getUserId());
		newUserDetail.setRoleId(picRequestDTO.getUserType());
		newUserDetail.setBirthday(ddMMYyyyFormat.parse(picRequestDTO.getBirthDate()));
		newUserDetail.setPlaceOfBirth(picRequestDTO.getBirthPlace());
		newUserDetail.setDeleted(0);
		newUserDetail.setPartnerCode(picRequestDTO.getPartnerCode());
		newUserDetail.setPartnerName(picRequestDTO.getPartnerName());

		userRepository.save(newUserDetail);


		return newUserDetail;
	}



	public BaseResponse showAllMenu(FilterPageAndDataDTO filterPageAndDataDTO) {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setHttpStatus(HttpStatus.OK);
		baseResponse.setMessage(OK.getMessage());
		baseResponse.setStatus(0);

		try {
			Pageable pageable = PageRequest.of(filterPageAndDataDTO.getPage() - 1, filterPageAndDataDTO.getDataCount());
			List<ParaMenuAdmin> menuAdmins = paraMenuAdminRepository
					.findAllMenuWithoutConcerningIsActiveWithPagination(pageable);
			if (menuAdmins.isEmpty())
				insertMenuAdminResponseWithDefaultValue(baseResponse);
			else
				insertListMenuAdminToResponse(baseResponse, menuAdmins);

		} catch (Exception e) {
			insertErrorResponse(baseResponse, ERROR_WHILE_GETTING_MENU_FROM_DB.getMessage(), e);
		}
		return baseResponse;
	}

	private void insertMenuAdminResponseWithDefaultValue(BaseResponse baseResponse) {
		MenuAdminHeadResponseDTO menuAdminHeadResponseDTO = new MenuAdminHeadResponseDTO();
		menuAdminHeadResponseDTO.setMenuList(new ArrayList<>());

		baseResponse.setData(menuAdminHeadResponseDTO);
	}

	private void insertListMenuAdminToResponse(BaseResponse baseResponse, List<ParaMenuAdmin> menuAdmins)
			throws JsonProcessingException {
		MenuAdminHeadResponseDTO menuAdminHeadResponseDTO = new MenuAdminHeadResponseDTO();
		List<MenuAdminDetailResponseDTO> menuAdminDetailResponseDTOS = new ArrayList<>();

		for (ParaMenuAdmin paraMenuAdmin : menuAdmins) {
			MenuAdminDetailResponseDTO menuAdminDetailResponseDTO = new MenuAdminDetailResponseDTO(
					paraMenuAdmin.getParaMenuCode(), paraMenuAdmin.getParaMenuAdminName(), paraMenuAdmin.getActive());
			menuAdminDetailResponseDTOS.add(menuAdminDetailResponseDTO);
		}

		menuAdminHeadResponseDTO.setMenuList(menuAdminDetailResponseDTOS);
		menuAdminHeadResponseDTO.setTotalMenu(menuAdmins.size());
		baseResponse.setData(menuAdminHeadResponseDTO);

		LOGGER.info(SUCCESS_GETTING_MENU_FROM_DB.getMessage(), objectMapper.writeValueAsString(baseResponse));
	}

	public BaseResponse showAllPic(FilterPageAndDataDTO filterPageAndDataDTO) {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setHttpStatus(HttpStatus.OK);
		baseResponse.setMessage(OK.getMessage());
		baseResponse.setStatus(0);

		Integer showAllPicIndicator = 0;

		try {

			Pageable userDetailPageable = PageRequest.of(filterPageAndDataDTO.getPage() - 1,
					filterPageAndDataDTO.getDataCount());
			List<UserDetail> userDetails = userRepository.findAllActiveUserByUsingPageable(userDetailPageable);
			if (userDetails.isEmpty())
				insertUserDetailResponseToDefaultValue(baseResponse);
			else
				insertAllUserDetailToResponse(baseResponse, userDetails, showAllPicIndicator);

			LOGGER.info(SUCCESS_GETTING_PIC_FROM_DB.getMessage(), objectMapper.writeValueAsString(baseResponse));
		} catch (Exception e) {
			insertErrorResponse(baseResponse, ERROR_WHILE_GETTING_PIC_FROM_DB.getMessage(), e);
		}
		return baseResponse;
	}

	// Show All PIC Indicator = 0, Searched PIC by Name Indicator = 1
	private void insertAllUserDetailToResponse(BaseResponse baseResponse, List<UserDetail> userDetails,
			Integer showAllOrSearchedPicIndicator) {
		AdminUserDetailsHeadResponseDTO adminUserDetailsHeadResponseDTO = new AdminUserDetailsHeadResponseDTO();
		List<UserDetailsResponseDTO> userDetailsResponseDTOS = new ArrayList<>();

		for (UserDetail userDetail : userDetails) {
			UserDetailsResponseDTO userDetailsResponseDTO = new UserDetailsResponseDTO(userDetail.getId(),
					userDetail.getUserId(), userDetail.getUsername(), userDetail.getEmail(), userDetail.getHandphone(),
					userDetail.getUserType(), userDetail.getPartnerCode(), userDetail.getPartnerName(),
					userDetail.getIsactive());

			userDetailsResponseDTOS.add(userDetailsResponseDTO);

			adminUserDetailsHeadResponseDTO.setUsers(userDetailsResponseDTOS);
			adminUserDetailsHeadResponseDTO
					.setTotalUser(showAllOrSearchedPicIndicator == 0 ? userRepository.countDataUserExceptDeletedUsers()
							: userDetailsResponseDTOS.size());

			baseResponse.setData(adminUserDetailsHeadResponseDTO);
		}
	}

	private void insertUserDetailResponseToDefaultValue(BaseResponse baseResponse) {
		AdminUserDetailsHeadResponseDTO adminUserDetailsHeadResponseDTO = new AdminUserDetailsHeadResponseDTO();
		adminUserDetailsHeadResponseDTO.setUsers(new ArrayList<>());

		baseResponse.setData(adminUserDetailsHeadResponseDTO);
	}

	public BaseResponse updateStatusPic(String userId) {
		BaseResponse baseResponse = new BaseResponse();

		baseResponse.setHttpStatus(HttpStatus.OK);
		baseResponse.setStatus(0);
		baseResponse.setMessage(OK.getMessage());

		try {
			UserDetail userDetail = userRepository.findUserByIdWithoutConcerningActive(userId);
			if (userDetail == null)
				insertToResponseWhenUserNotFound(baseResponse, userId);
			else
				updatePicActive(baseResponse, userDetail);
		} catch (Exception e) {
			insertErrorResponse(baseResponse, ERROR_WHILE_NON_ACTIVING_PIC.getMessage(), e);
		}
		return baseResponse;
	}

	private void insertToResponseWhenUserNotFound(BaseResponse baseResponse, String userId) {
		baseResponse.setMessage(USER_NOT_FOUND_WITH_USERNAME.getMessage() + userId);
		baseResponse.setData(null);
	}

	private void updatePicActive(BaseResponse baseResponse, UserDetail userDetail) {
		if (userDetail.getIsactive() == 0)
			userRepository.nonActivingUserBasedOnUserId(userDetail.getUserId());
		else
			userRepository.activingUserBasedOnUserId(userDetail.getUserId());

		baseResponse.setData(SUCCESS_UPDATING_DATA.getMessage());
	}

	public BaseResponse insertOrUpdateMenu(AdminMenuRequestDTO adminMenuRequestDTO) {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setHttpStatus(HttpStatus.OK);
		baseResponse.setStatus(0);
		baseResponse.setMessage(OK.getMessage());

		try {
			if (!adminMenuRequestDTO.getMenuCode().substring(0, 3).equals(MENU_ID_PREFIX.getMessage())) {
				baseResponse.setMessage(INVALID_MENU_CODE.getMessage());
				baseResponse.setData(null);
			} else
				mapMenu(baseResponse, adminMenuRequestDTO);
		} catch (Exception e) {
			insertErrorResponse(baseResponse, ERROR_WHILE_INSERTING_OR_UPDATING_MENU.getMessage(), e);
		}
		return baseResponse;
	}

	private void mapMenu(BaseResponse baseResponse, AdminMenuRequestDTO adminMenuRequestDTO)
			throws JsonProcessingException {
		ParaMenuAdmin paraMenuAdmin = paraMenuAdminRepository
				.findParaMenuAdminBasedOnMenuCode(adminMenuRequestDTO.getMenuCode());
		if (paraMenuAdmin == null)
			insertNewMenuIntoDatabase(baseResponse, adminMenuRequestDTO);
		else
			updateAnExistingMenu(baseResponse, adminMenuRequestDTO, paraMenuAdmin);
	}

	private void updateAnExistingMenu(BaseResponse baseResponse, AdminMenuRequestDTO adminMenuRequestDTO,
			ParaMenuAdmin paraMenuAdmin) throws JsonProcessingException {
		paraMenuAdmin.setParaMenuAdminName(adminMenuRequestDTO.getMenuName());
		paraMenuAdminRepository.save(paraMenuAdmin);

		baseResponse.setData(SUCCESS_UPDATING_DATA.getMessage());

		LOGGER.info(SUCCESS_UPDATE_MENU.getMessage(), objectMapper.writeValueAsString(baseResponse));
	}

	private void insertNewMenuIntoDatabase(BaseResponse baseResponse, AdminMenuRequestDTO adminMenuRequestDTO)
			throws JsonProcessingException {
		ParaMenuAdmin newMenu = new ParaMenuAdmin();
		newMenu.setParaMenuCode(adminMenuRequestDTO.getMenuCode());
		newMenu.setParaMenuAdminName(adminMenuRequestDTO.getMenuName());
		newMenu.setActive(0);
		newMenu.setDeleted(0);
		newMenu.setCreatedBy(ADMIN.getMessage());
		newMenu.setModifiedBy(ADMIN.getMessage());

		paraMenuAdminRepository.save(newMenu);
		baseResponse.setData(newMenu);

		LOGGER.info(SUCCESS_SAVING_NEW_MENU_INTO_DB.getMessage(), objectMapper.writeValueAsString(baseResponse));
	}

	public BaseResponse insertOrUpdateRole(RoleRequestDTO roleRequestDTO) {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setHttpStatus(HttpStatus.OK);
		baseResponse.setStatus(0);
		baseResponse.setMessage(OK.getMessage());

		try {
			mapRole(baseResponse, roleRequestDTO);
		} catch (Exception e) {
			insertErrorResponse(baseResponse, ERROR_WHILE_INSERTING_OR_UPDATING_ROLE.getMessage(), e);
		}
		return baseResponse;
	}

	private void mapRole(BaseResponse baseResponse, RoleRequestDTO roleRequestDTO) throws JsonProcessingException {
		RoleDetail roleDetail = roleRepository.findUndeletedUserRoleBasedOnRoleId(roleRequestDTO.getRoleId());
		updateAnExistingRole(roleDetail, baseResponse, roleRequestDTO);
	}

	private void updateAnExistingRole(RoleDetail currentRole, BaseResponse baseResponse, RoleRequestDTO roleRequestDTO)
			throws JsonProcessingException {
		currentRole.setRoleName(roleRequestDTO.getRoleName());
		currentRole.setRoleId(roleRequestDTO.getRoleId());
		currentRole.setRoleCode(roleRequestDTO.getPartnerType());
		currentRole.setModifyBy(ADMIN.getMessage());

		roleRepository.save(currentRole);

		paraRoleMenuRepository.deleteMenuByRoleId(currentRole.getParaRoleId());
		mapMenuAndRoleBasedOnCheckedOrUncheckedMenu(roleRequestDTO.getCheckedMenuCodes(), currentRole);

		baseResponse.setData(SUCCESS_UPDATING_DATA.getMessage());
		LOGGER.info(SUCCESS_UPDATING_ROLE.getMessage(), objectMapper.writeValueAsString(baseResponse));
	}

	private void insertNewRoleIntoDatabase(RoleDetail newRole, BaseResponse baseResponse, RoleRequestDTO roleRequestDTO)
			throws JsonProcessingException {
		newRole.setRoleName(roleRequestDTO.getRoleName());
		newRole.setRoleCode(roleRequestDTO.getPartnerType());
		newRole.setRoleId(roleRequestDTO.getRoleId());
		newRole.setIsActive(0);
		newRole.setDeleted(0);
		newRole.setCreateBy(ADMIN.getMessage());
		newRole.setModifyBy(ADMIN.getMessage());

		roleRepository.save(newRole);
		mapMenuAndRoleBasedOnCheckedOrUncheckedMenu(roleRequestDTO.getCheckedMenuCodes(), newRole);

		baseResponse.setData(newRole);
		LOGGER.info(SUCCESS_SAVING_NEW_ROLE_INTO_DB.getMessage(), objectMapper.writeValueAsString(baseResponse));
	}

	// Checked Menu Indicator (Eligible Menu) = 0, Unchecked Menu (Not Eligible
	// Menu) Indicator = 1
	private void insertToTableParaRoleMenuWhenThereIsAnUnmappedMenuWhenUpdate(ParaRoleMenu paraRoleMenu,
			ParaMenuAdmin paraMenuAdmin, RoleDetail roleDetail, Integer checkedOrUncheckedIndicator) {
		paraRoleMenu.setParaMenuAdmin(paraMenuAdmin);
		paraRoleMenu.setRoleDetail(roleDetail);
		paraRoleMenu.setActive(checkedOrUncheckedIndicator == 0 ? 0 : 1);
		paraRoleMenu.setCreatedBy(ADMIN.getMessage());
		paraRoleMenu.setModifiedBy(ADMIN.getMessage());

		paraRoleMenuRepository.save(paraRoleMenu);
	}



	private void mapMenuAndRoleBasedOnCheckedOrUncheckedMenu(List<String> checkedMenuCodes, RoleDetail roleDetail) {
		for (String checkedMenuCode : checkedMenuCodes) {

			Integer checkedMenuFlag = 0;
			ParaMenuAdmin paraMenuAdmin = paraMenuAdminRepository.findParaMenuAdminBasedOnMenuCode(checkedMenuCode);
			ParaRoleMenu paraRoleMenu = paraRoleMenuRepository.findParaRoleMenuBasedOnParaRoleIdAndMenuAdminId(
					roleDetail.getParaRoleId(), paraMenuAdmin.getParaMenuAdminId());

			if (paraRoleMenu == null)
				insertToTableParaRoleMenuWhenThereIsAnUnmappedMenuWhenUpdate(new ParaRoleMenu(), paraMenuAdmin,
						roleDetail, checkedMenuFlag);

		}
	}

	public BaseResponse generateMenuId() {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setHttpStatus(HttpStatus.OK);
		baseResponse.setStatus(0);
		baseResponse.setMessage(OK.getMessage());

		try {
			List<ParaMenuAdmin> paraMenuAdmins = paraMenuAdminRepository.findAllMenuOrderByMenuCodeDesc();
			baseResponse.setData(formatUtil.generateMenuId(Integer.valueOf(paraMenuAdmins.get(0).getParaMenuCode()
					.substring(paraMenuAdmins.get(0).getParaMenuCode().length() - 3))));

			// Take 3 digits from behind to generate menuId, then increase it by 1 for new
			// menu
			baseResponse.setData(formatUtil.generateMenuId(Integer.valueOf(paraMenuAdmins.get(0).getParaMenuCode()
					.substring(paraMenuAdmins.get(0).getParaMenuCode().length() - 3))));
		} catch (Exception e) {
			insertErrorResponse(baseResponse, ERROR_WHILE_GENERATING_ID.getMessage(), e);
		}
		return baseResponse;
	}

	public BaseResponse generateRoleId() {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setHttpStatus(HttpStatus.OK);
		baseResponse.setStatus(0);
		baseResponse.setMessage(OK.getMessage());

		try {
			List<RoleDetail> roleDetails = roleRepository.findAllRoleDetailsOrderByRoleCodeDesc();
			// Take 3 digits from behind, Then increase by 1 to generate new RoleId.
			Integer roleCodeSubstring = Integer
					.valueOf(roleDetails.get(0).getRoleCode().substring(roleDetails.get(0).getRoleCode().length() - 3));
			baseResponse.setData(formatUtil.generateRoleId(roleCodeSubstring));
		} catch (Exception e) {
			insertErrorResponse(baseResponse, ERROR_WHILE_GENERATING_ID.getMessage(), e);
		}
		return baseResponse;
	}

	public BaseResponse showDetailPicByUserId(DetailPicRequestDTO detailPicRequestDTO) {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setHttpStatus(HttpStatus.OK);
		baseResponse.setMessage(OK.getMessage());
		baseResponse.setStatus(0);

		try {
			mapDetailPic(detailPicRequestDTO, baseResponse);
		} catch (Exception e) {
			insertErrorResponse(baseResponse, ERROR_WHILE_SHOWING_DETAIL_PIC.getMessage(), e);
		}
		return baseResponse;
	}

	private void mapDetailPic(DetailPicRequestDTO detailPicRequestDTO, BaseResponse baseResponse) {
		UserDetail userDetail = userRepository
				.findUserByUserIdAndIdWithoutConcerningActive(detailPicRequestDTO.getUserId());

		if (userDetail == null) {
			baseResponse.setData(null);
			baseResponse.setMessage(USER_NOT_FOUND_WITH_USERNAME.getMessage() + detailPicRequestDTO.getUserId());
		} else {
			RoleDetail roleDetail = roleRepository.findUserRoleBasedOnRoleCode(userDetail.getRoleId());
			List<CheckedOrUncheckedMenuResponseDTO> checkedMenus = new ArrayList<>();

			List<ParaUserDetailMenu> paraUserDetailMenus = paraUserDetailMenuRepository
					.findAllUserDetailMenuById(userDetail.getId());
			for (ParaUserDetailMenu paraUserDetailMenu : paraUserDetailMenus) {
				System.out.println(paraUserDetailMenu.getParaMenuAdmin().getParaMenuAdminName());
				mapCheckAndUncheckedMenuForUserAndMenu(paraUserDetailMenu, checkedMenus);
			}

			PicResponseDTO picResponseDTO = new PicResponseDTO(userDetail.getId(), userDetail.getUserId(),
					userDetail.getUsername(), userDetail.getPlaceOfBirth(), userDetail.getBirthday(),
					userDetail.getHandphone(), userDetail.getNoWa(), roleDetail.getRoleCode(), userDetail.getUserType(),
					userDetail.getPartnerCode(), userDetail.getPartnerName(), userDetail.getIsactive(), checkedMenus);
			baseResponse.setData(picResponseDTO);
		}
	}

	private void mapCheckAndUncheckedMenuForUserAndMenu(ParaUserDetailMenu paraUserDetailMenu,
			List<CheckedOrUncheckedMenuResponseDTO> checkedMenus) {

		ParaMenuAdmin paraMenuAdmin = paraMenuAdminRepository
				.findParaMenuAdminBasedOnMenuId(paraUserDetailMenu.getParaMenuAdmin().getParaMenuAdminId());

		if (paraUserDetailMenu.getActive() == 0)
			checkedMenus.add(insertCheckedOrUncheckedMenuForUserAndMenu(paraMenuAdmin));
	}

	private CheckedOrUncheckedMenuResponseDTO insertCheckedOrUncheckedMenuForUserAndMenu(ParaMenuAdmin paraMenuAdmin) {
		return new CheckedOrUncheckedMenuResponseDTO(paraMenuAdmin.getParaMenuCode(),
				paraMenuAdmin.getParaMenuAdminName());
	}

	public BaseResponse showAllRole(FilterPageAndDataDTO filterPageAndDataDTO) {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setHttpStatus(HttpStatus.OK);
		baseResponse.setStatus(0);
		baseResponse.setMessage(OK.getMessage());

		try {
			Pageable pageable = PageRequest.of(filterPageAndDataDTO.getPage() - 1, filterPageAndDataDTO.getDataCount());
			List<RoleDetail> roles = roleRepository.findAllUserRole(pageable);

			if (roles.isEmpty())
				baseResponse.setData(new RoleHeadResponseDTO());
			else
				insertAllRolesToResponse(roles, baseResponse);
		} catch (Exception e) {
			insertErrorResponse(baseResponse, ERROR_WHILE_GETTING_ROLE_FROM_DB.getMessage(), e);
		}
		return baseResponse;
	}

	private void insertAllRolesToResponse(List<RoleDetail> roles, BaseResponse baseResponse)
			throws JsonProcessingException {
		RoleHeadResponseDTO roleHeadResponseDTO = new RoleHeadResponseDTO();
		List<AllRoleResponseDTO> allRoleResponseDTOS = new ArrayList<>();

		for (RoleDetail role : roles) {
			AllRoleResponseDTO allRoleResponseDTO = new AllRoleResponseDTO(role.getRoleCode(), role.getRoleId(),
					role.getRoleName(), role.getIsActive());

			allRoleResponseDTOS.add(allRoleResponseDTO);
		}
		roleHeadResponseDTO.setRoles(allRoleResponseDTOS);
		roleHeadResponseDTO.setTotalRole(roles.size());
		baseResponse.setData(roleHeadResponseDTO);

		LOGGER.info(SUCCESS_GETTING_ROLE_FROM_DB.getMessage(), objectMapper.writeValueAsString(baseResponse));
	}

	public BaseResponse updateStatusRole(String roleId) {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setHttpStatus(HttpStatus.OK);
		baseResponse.setStatus(0);
		baseResponse.setMessage(OK.getMessage());

		try {
			RoleDetail roleDetail = roleRepository.findUndeletedUserRoleBasedOnRoleId(roleId);
			if (roleDetail == null) {
				baseResponse.setMessage(CANNOT_FIND_ROLE.getMessage() + roleId);
				baseResponse.setData(null);
			} else
				setActiveOrNonActiveToRole(roleDetail, baseResponse);

		} catch (Exception e) {
			insertErrorResponse(baseResponse, ERROR_WHILE_UPDATING_STATUS_ROLE.getMessage(), e);
		}
		return baseResponse;
	}

	private void setActiveOrNonActiveToRole(RoleDetail roleDetail, BaseResponse baseResponse)
			throws JsonProcessingException {
		if (roleDetail.getIsActive() == 0)
			roleRepository.nonActivateRole(roleDetail.getRoleId());
		else
			roleRepository.activateRole(roleDetail.getRoleId());

		baseResponse.setData(roleDetail);

		LOGGER.info(SUCCESS_UPDATING_ROLE.getMessage(), objectMapper.writeValueAsString(baseResponse));
	}

	public BaseResponse showDetailMenuBasedOnMenuCode(String menuCode) {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setHttpStatus(HttpStatus.OK);
		baseResponse.setStatus(0);
		baseResponse.setMessage(OK.getMessage());

		try {
			mapMenuDetail(baseResponse, menuCode);
		} catch (Exception e) {
			insertErrorResponse(baseResponse, ERROR_WHILE_SHOWING_DETAIL_MENU.getMessage(), e);
		}
		return baseResponse;
	}

	private void mapMenuDetail(BaseResponse baseResponse, String menuCode) {
		ParaMenuAdmin paraMenuAdmin = paraMenuAdminRepository.findMenuByMenuCode(menuCode);
		if (paraMenuAdmin == null) {
			baseResponse.setMessage(CANNOT_FIND_MENU.getMessage() + menuCode);
			baseResponse.setData(null);
		} else {
			MenuAdminDetailResponseDTO menuAdminDetailResponseDTO = new MenuAdminDetailResponseDTO(
					paraMenuAdmin.getParaMenuCode(), paraMenuAdmin.getParaMenuAdminName(), paraMenuAdmin.getActive());
			baseResponse.setData(menuAdminDetailResponseDTO);
		}
	}

	public BaseResponse showDetailRoleBasedOnRoleCode(String roleCode) {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setHttpStatus(HttpStatus.OK);
		baseResponse.setStatus(0);
		baseResponse.setMessage(OK.getMessage());

		try {
			mapDetailRole(roleCode, baseResponse);
		} catch (Exception e) {
			insertErrorResponse(baseResponse, ERROR_WHILE_SHOWING_DETAIL_ROLE.getMessage(), e);
		}
		return baseResponse;
	}

	private void mapDetailRole(String roleCode, BaseResponse baseResponse) {
		RoleDetail roleDetail = roleRepository.findUserRoleBasedOnRoleCode(roleCode);
		if (roleDetail == null) {
			baseResponse.setData(null);
			baseResponse.setMessage(CANNOT_FIND_ROLE.getMessage() + roleCode);
		} else {
			List<CheckedOrUncheckedMenuResponseDTO> checkedMenus = new ArrayList<>();

			List<ParaRoleMenu> paraRoleMenus = paraRoleMenuRepository
					.findParaRoleMenuByParaRoleId(roleDetail.getParaRoleId());
			for (ParaRoleMenu paraRoleMenu : paraRoleMenus) {
				if (paraRoleMenu.getParaMenuAdmin().getDeleted() == 0)
					mapCheckAndUncheckedMenuForUserDetailRole(paraRoleMenu, checkedMenus);
			}

			RoleDetailResponseDTO roleDetailResponseDTO = new RoleDetailResponseDTO(roleDetail.getRoleCode(),
					roleDetail.getRoleId(), roleDetail.getRoleName(), roleDetail.getIsActive(), checkedMenus);
			baseResponse.setData(roleDetailResponseDTO);
		}
	}

	private void mapCheckAndUncheckedMenuForUserDetailRole(ParaRoleMenu paraRoleMenu,
			List<CheckedOrUncheckedMenuResponseDTO> checkedMenus) {

		ParaMenuAdmin paraMenuAdmin = paraMenuAdminRepository
				.findParaMenuAdminBasedOnMenuId(paraRoleMenu.getParaMenuAdmin().getParaMenuAdminId());
		if (paraMenuAdmin.getDeleted() == 0) {
			if (paraRoleMenu.getActive() == 0)
				checkedMenus.add(insertCheckedOrUncheckedMenuForRoleAndMenu(paraMenuAdmin));
		}
	}

	private CheckedOrUncheckedMenuResponseDTO insertCheckedOrUncheckedMenuForRoleAndMenu(ParaMenuAdmin paraMenuAdmin) {
		return new CheckedOrUncheckedMenuResponseDTO(paraMenuAdmin.getParaMenuCode(),
				paraMenuAdmin.getParaMenuAdminName());
	}

	public BaseResponse searchPicByName(String picName) {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setHttpStatus(HttpStatus.OK);
		baseResponse.setStatus(0);
		baseResponse.setMessage(OK.getMessage());

		Integer searchedPicByNameIndicator = 1;

		try {
			if (picName.length() < 3) {
				baseResponse.setMessage(SEARCHED_PIC_NAME_MUST_BE_AT_LEAST_THREE_CHARACTER.getMessage());
				baseResponse.setData(null);
			} else
				mapPicByName(baseResponse, picName, searchedPicByNameIndicator);
		} catch (Exception e) {
			insertErrorResponse(baseResponse, ERROR_WHILE_SEARCHING_PIC_BY_NAME.getMessage(), e);
		}
		return baseResponse;
	}

	private void mapPicByName(BaseResponse baseResponse, String picName, Integer indicator) {
		List<UserDetail> userDetails = userRepository.findUserByUsername(picName.toLowerCase());
		if (userDetails.isEmpty()) {
			baseResponse.setMessage(USER_NOT_FOUND_WITH_USERNAME.getMessage() + picName);
			baseResponse.setData(null);
		} else
			insertAllUserDetailToResponse(baseResponse, userDetails, indicator);
	}

	public BaseResponse updateStatusMenu(String menuCode) {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setHttpStatus(HttpStatus.OK);
		baseResponse.setStatus(0);
		baseResponse.setMessage(OK.getMessage());

		try {
			ParaMenuAdmin paraMenuAdmin = paraMenuAdminRepository.findParaMenuAdminBasedOnMenuCode(menuCode);
			if (paraMenuAdmin == null) {
				baseResponse.setMessage(CANNOT_FIND_MENU.getMessage() + menuCode);
				baseResponse.setData(null);
			} else
				updateParaMenuAdminToActiveOrInactive(paraMenuAdmin, baseResponse);
		} catch (Exception e) {
			insertErrorResponse(baseResponse, ERROR_WHILE_UPDATING_MENU_STATUS.getMessage(), e);
		}
		return baseResponse;
	}

	private void updateParaMenuAdminToActiveOrInactive(ParaMenuAdmin paraMenuAdmin, BaseResponse baseResponse)
			throws JsonProcessingException {
		if (paraMenuAdmin.getActive() == 0)
			paraMenuAdminRepository.nonActivingMenuByParaMenuAdminId(paraMenuAdmin.getParaMenuAdminId());
		else
			paraMenuAdminRepository.activingMenuByParaMenuAdminId(paraMenuAdmin.getParaMenuAdminId());

		baseResponse.setData(SUCCESS_UPDATING_DATA.getMessage());
		LOGGER.info(SUCCESS_UPDATE_MENU.getMessage(), objectMapper.writeValueAsString(baseResponse));
	}

	public BaseResponse generateUserId() {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setHttpStatus(HttpStatus.OK);
		baseResponse.setMessage(OK.getMessage());
		baseResponse.setStatus(0);

		try {
			baseResponse.setData(userRepository.countDataUser() + 1);
		} catch (Exception e) {
			insertErrorResponse(baseResponse, ERROR_WHILE_GENERATING_USER_ID.getMessage(), e);
		}
		return baseResponse;
	}

	public BaseResponse insertDataUser(List<UserDataInjectDTO> insertDataInject) {
		var baseResponse = new BaseResponse();
		baseResponse.setHttpStatus(HttpStatus.OK);
		baseResponse.setMessage(OK.getMessage());
		baseResponse.setStatus(0);
		try {
			List<UserDataInject> userDataInjectEntity = insertDataInject.stream()
					.map(e -> modelMapper.map(e, UserDataInject.class)).collect(Collectors.toList());
			
			List<UserDataInjectHist> userDataInjectHistEntity = insertDataInject.stream()
					.map(e -> modelMapper.map(e, UserDataInjectHist.class)).collect(Collectors.toList());
			
			//Save Into Table Temporary
			userDataInjectRepository.saveAll(userDataInjectEntity);
			//Insert Mapping Into Primary Table
			userDataInjectRepository.mappingIntoMainTableUser();
			//Delete All Data From Table Temporary
			userDataInjectRepository.deleteAll();
			//Insert Into Table History Temporary
			userDataInjectHistRepository.saveAll(userDataInjectHistEntity);
			

			for (UserDataInject userDataInject : userDataInjectEntity) {
				List<String> checkedMenus = new ArrayList<>();

				UserDetail currentUserDetail = userRepository.findUserByUserIdAndIdWithoutConcerningActive(userDataInject.getEmail());
				RoleDetail roleDetail = roleRepository.findUserRoleBasedOnRoleCode(currentUserDetail.getRoleId());
				
				if (roleDetail == null) {
					baseResponse.setData(null);
					baseResponse.setMessage(CANNOT_FIND_ROLE.getMessage() + currentUserDetail.getRoleId());
				} else {
					List<ParaRoleMenu> paraRoleMenus = paraRoleMenuRepository
							.findParaRoleMenuByParaRoleId(roleDetail.getParaRoleId());
					for (ParaRoleMenu paraRoleMenu : paraRoleMenus) {
						checkedMenus.add(paraRoleMenu.getParaMenuAdmin().getParaMenuCode());
					}
					insertMenuEligibleForPic(currentUserDetail, checkedMenus);
				}

			}

		} catch (Exception e) {
			insertErrorResponse(baseResponse, ERROR_WHILE_SAVING_PIC_TO_DB.getMessage(), e);
		}
		return baseResponse;
	}

	public BaseResponse deleteMenuByMenuCode(String menuCode) {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setHttpStatus(HttpStatus.OK);
		baseResponse.setMessage(OK.getMessage());
		baseResponse.setStatus(0);

		try {
			ParaMenuAdmin paraMenuAdmin = paraMenuAdminRepository.findParaMenuAdminBasedOnMenuCode(menuCode);
			if (paraMenuAdmin == null) {
				baseResponse.setMessage(CANNOT_FIND_MENU.getMessage() + menuCode);
				baseResponse.setData(null);
			} else {
				paraMenuAdminRepository.nonActivingMenuByParaMenuAdminId(paraMenuAdmin.getParaMenuAdminId());
				paraMenuAdminRepository.setStatusDeletedByMenuId(paraMenuAdmin.getParaMenuAdminId());
				baseResponse.setData(SUCCESS_DELETING_MENU.getMessage());
			}
		} catch (Exception e) {
			insertErrorResponse(baseResponse, ERROR_WHILE_DELETING_MENU.getMessage(), e);
		}
		return baseResponse;
	}

	public BaseResponse deletePicById(Integer id) {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setHttpStatus(HttpStatus.OK);
		baseResponse.setMessage(OK.getMessage());
		baseResponse.setStatus(0);

		try {
			UserDetail userDetail = userRepository.findUserById(id);
			if (userDetail == null) {
				baseResponse.setMessage(USER_NOT_FOUND_WITH_ID.getMessage());
				baseResponse.setData(null);
			} else {
				userRepository.nonActivingUserBasedOnUserId(userDetail.getUserId());
				userRepository.updateIsDeletedToOne(userDetail.getUserId());

				LOGGER.info(SUCCESS_DELETING_PIC.getMessage());
				baseResponse.setData(SUCCESS_DELETING_PIC.getMessage());
			}
		} catch (Exception e) {
			insertErrorResponse(baseResponse, ERROR_WHILE_DELETING_PIC.getMessage(), e);
		}
		return baseResponse;
	}

	public BaseResponse deleteRoleByRoleId(String roleId) {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setHttpStatus(HttpStatus.OK);
		baseResponse.setMessage(OK.getMessage());
		baseResponse.setStatus(0);

		try {
			roleRepository.nonActivateRole(roleId);
			roleRepository.setToDeletedByRoleCode(roleId);
			baseResponse.setData(SUCCESS_DELETING_ROLE.getMessage());
		} catch (Exception e) {
			insertErrorResponse(baseResponse, ERROR_WHILE_DELETING_ROLE.getMessage(), e);
		}
		return baseResponse;
	}

	public BaseResponse getPartnerType() {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setHttpStatus(HttpStatus.OK);
		baseResponse.setMessage(OK.getMessage());
		baseResponse.setStatus(0);

		try {
			List<ParaPartnerTypeDTO> listPartnerType = paraPartnerTypeRepository.findAllActivePartnerType();
			if (listPartnerType.isEmpty()) {
				baseResponse.setStatus(1);
				baseResponse.setMessage(DATA_NOT_FOUND.getMessage());
			} else {
				baseResponse.setData(listPartnerType);
			}

		} catch (Exception e) {
			insertErrorResponse(baseResponse, ERROR_WHILE_GET_DATA.getMessage(), e);
		}
		return baseResponse;
	}

	public BaseResponse getRoleByPartnerType(String partnerType) {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setHttpStatus(HttpStatus.OK);
		baseResponse.setStatus(0);
		baseResponse.setMessage(OK.getMessage());

		try {
			List<RoleDetail> roles = roleRepository.findRoleByPartnerType(partnerType);

			if (roles.isEmpty())
				baseResponse.setData(new RoleHeadResponseDTO());
			else
				insertAllRolesToResponse(roles, baseResponse);
		} catch (Exception e) {
			insertErrorResponse(baseResponse, ERROR_WHILE_GETTING_ROLE_FROM_DB.getMessage(), e);
		}
		return baseResponse;
	}

	public BaseResponse insertNewPic(PicRequestDTO picRequestDTO) {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setHttpStatus(HttpStatus.OK);
		baseResponse.setStatus(0);
		baseResponse.setMessage(OK.getMessage());

		try {
			UserDetail checkUserDetail = userRepository.findUserByUserId(picRequestDTO.getUserId());
			if (checkUserDetail == null)
				mapNewUserDetail(picRequestDTO, baseResponse);
			else {
				baseResponse.setMessage(USER_ID_ALREADY_EXISTS.getMessage());
				baseResponse.setData(null);
			}
		} catch (Exception e) {
			insertErrorResponse(baseResponse, ERROR_WHILE_INSERTING_NEW_PIC.getMessage(), e);
		}
		return baseResponse;
	}

	public BaseResponse insertNewRole(RoleRequestDTO roleRequestDTO) {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setHttpStatus(HttpStatus.OK);
		baseResponse.setStatus(0);
		baseResponse.setMessage(OK.getMessage());

		try {
			RoleDetail roleDetail = roleRepository.findUndeletedUserRoleBasedOnRoleId(roleRequestDTO.getRoleId());
			if (roleDetail == null)
				insertNewRoleIntoDatabase(new RoleDetail(), baseResponse, roleRequestDTO);
			else {
				baseResponse.setMessage(ROLE_ID_ALREADY_EXISTS.getMessage());
				baseResponse.setData(null);
			}

		} catch (Exception e) {
			insertErrorResponse(baseResponse, ERROR_WHILE_INSERTING_NEW_ROLE.getMessage(), e);
		}
		return baseResponse;
	}
}
