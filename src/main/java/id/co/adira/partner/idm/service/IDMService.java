package id.co.adira.partner.idm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import id.co.adira.partner.idm.dto.*;
import id.co.adira.partner.idm.entity.*;
import id.co.adira.partner.idm.exception.AdiraCustomException;
import id.co.adira.partner.idm.repository.*;
import id.co.adira.partner.idm.utils.DateUtil;
import id.co.adira.partner.idm.utils.PortfolioMappingUtil;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.*;

import static id.co.adira.partner.idm.constant.AppEnum.*;
import static id.co.adira.partner.idm.constant.LoggerEnum.ERROR_WHILE_GETTING_ELIGIBLE_MENU_FOR_USER;
import static id.co.adira.partner.idm.constant.StatusEnum.*;

@Service
public class IDMService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private PartnerUrlRepository partnerUrlRepository;

	@Autowired
	private InfoAppDetailRepository infoAppDetailRepository;

	@Autowired
	private LoginUserDetailRepository loginUserDetailRepository;

	@Autowired
	private PortfolioMappingUtil portfolioMappingUtil;

	@Autowired
	private ParaObjectRepository paraObjectRepository;

	@Autowired
	private ParaUserDetailMenuRepository paraUserDetailMenuRepository;

	@Autowired
	private ParaMenuAdminRepository paraMenuAdminRepository;


	@Value("${url.api.auth}")
	private String urlAuth;

	@Value("${url.api.changePassword}")
	private String urlChangePassword;

	@Value("${url.api.resetPassword}")
	private String urlResetPassword;

	@Value("${url.api.getProperty}")
	private String urlGetProperty;

	@Autowired
	RestTemplate restTemplate;

	private static final Logger LOGGER = LoggerFactory.getLogger(IDMService.class);

	ModelMapper modelMapper = new ModelMapper();
	String error = "";

	public StandardResponseDTO authUser(UserLoginRequest params) throws AdiraCustomException {
		StandardResponseDTO resultx = new StandardResponseDTO();

		restTemplate = new RestTemplate();

		ResponseEntity<String> responseEntity;
		try {
			responseEntity = restTemplate.postForEntity(urlAuth, params, String.class);
			String json = String.valueOf(responseEntity.getBody());

			ObjectMapper objectMapper = new ObjectMapper();
			resultx = objectMapper.readValue(json, StandardResponseDTO.class);

		} catch (Exception e) {
			String hasil = FAILED_AUTH.getMessage() + e.getMessage();
			throw new AdiraCustomException(hasil, e);
		}

		return resultx;
	}

	public StandardResponseDTO resetPassword(ResetPasswordRequest params) throws AdiraCustomException {
		StandardResponseDTO result = new StandardResponseDTO();

		restTemplate = new RestTemplate();

		ResponseEntity<String> responseEntity;
		try {
			responseEntity = restTemplate.postForEntity(urlResetPassword, params, String.class);
			String json = String.valueOf(responseEntity.getBody());

			ObjectMapper objectMapper = new ObjectMapper();
			result = objectMapper.readValue(json, StandardResponseDTO.class);

		} catch (Exception e) {
			String hasil = FAILED_RESET_PASSWORD.getMessage() + e.getMessage();
			throw new AdiraCustomException(hasil, e);
		}

		return result;
	}

	public StandardResponseDTO changePassword(ChangePasswordRequest params) throws AdiraCustomException {
		StandardResponseDTO result = new StandardResponseDTO();

		restTemplate = new RestTemplate();

		ResponseEntity<String> responseEntity;
		try {
			responseEntity = restTemplate.postForEntity(urlChangePassword, params, String.class);
			String json = String.valueOf(responseEntity.getBody());

			ObjectMapper objectMapper = new ObjectMapper();
			result = objectMapper.readValue(json, StandardResponseDTO.class);

		} catch (Exception e) {
			String hasil = FAILED_CHANGE_PASSWORD.getMessage() + e.getMessage();
			throw new AdiraCustomException(hasil, e);
		}

		return result;
	}

	public UserDetailDTO findUserByIdV1(String userID) throws AdiraCustomException {
		UserDetail data = new UserDetail();
		UserDetailDTO detail = new UserDetailDTO();
		try {
			LOGGER.info(FIND_USER_BY_ID.getMessage() + userID);
			data = userRepository.findUserByUserId(userID);
			LOGGER.info(data.getUserId());
		} catch (Exception e) {
			error = FAILED_GET_USER_BY_ID.getMessage() + e.getMessage();
			throw new AdiraCustomException(error, e);
		}
		if (data.getUserId() != null) {
			detail = modelMapper.map(data, UserDetailDTO.class);
		}
		return detail;
	}

	public UserDetailDTOV2 findUserByIdV2(String userID) throws AdiraCustomException{
		UserDetail data = new UserDetail();
		UserDetailDTOV2 userDetail = new UserDetailDTOV2();
		try {
			LOGGER.info(FIND_USER_BY_ID.getMessage() + userID);
			data = userRepository.findUserByUserId(userID);
			LOGGER.info(data.getUserId());
		} catch (Exception e) {
			error = FAILED_GET_USER_BY_ID.getMessage() + e.getMessage();
			throw new AdiraCustomException(error, e);
		}
		if (data.getUserId() != null) {
			userDetail = modelMapper.map(data, UserDetailDTOV2.class);
			userDetail.setPortfolios(insertPortfolio(data));
		}
		return userDetail;
	}

	public List<PortfolioDTO> insertPortfolio(UserDetail data){
		List<PortfolioDTO> portfolios = new ArrayList<>();
		String[] objectCodeAndDesc = new String[2];

		if(data.getUserType().equals(AXI.getMessage()) || data.getUserType().equals(KEDAY.getMessage())) {
			objectCodeAndDesc = portfolioMappingUtil.portfolioMappingFromTable().get(data.getPortfolio()).split("-");
			portfolios.add(insertPortfolioDetail(data, objectCodeAndDesc));
		}
		else{
			portfolios = paraObjectRepository.getPortfolio(data.getPartnerCode());
		}
		return portfolios;
	}

	private PortfolioDTO insertPortfolioDetail(UserDetail data, String[] objectCodeAndDesc) {
		PortfolioDTO portfolioDTO = new PortfolioDTO();

		portfolioDTO.setObjectCode(objectCodeAndDesc[0]);
		portfolioDTO.setObjectDesc(objectCodeAndDesc[1]);

		return portfolioDTO;
	}

	public String updateProfile(UpdateProfileDTO userDTO) throws AdiraCustomException {
		String response = "";
		try {
			LOGGER.info(FIND_USER_BY_ID.getMessage() + userDTO.getUserId());
			UserDetail data = userRepository.findUserByUserId(userDTO.getUserId());
			if (data == null) {
				LOGGER.info(FAILED_GET_USER_BY_ID.getMessage(), userDTO.getUserId());
				response = DATA_NOT_FOUND.getMessage();

			} else {

				data.setUsername(userDTO.getUserName());
				data.setEmail(userDTO.getEmail());
				data.setHandphone(userDTO.getPhoneNumber());
				data.setNoWa(userDTO.getWaNumber());

				data.setModifyDate(new Date());
				data.setModifyBy(userDTO.getUserId());

				userRepository.save(data);
				LOGGER.info(UPDATE_PROFILE_WITH_ID.getMessage() + userDTO.getUserId());
				response = UPDATE_SUCCESS.getMessage();

			}
		} catch (Exception e) {
			error = FAILED_UPDATE_PROFILE.getMessage() + e.getMessage();
			throw new AdiraCustomException(error, e);
		}

		return response;
	}

	public VersionDTO getInfoApps() {
		VersionDTO versionDTO = new VersionDTO();

		InfoAppDetail infoAppDetail = infoAppDetailRepository.findAllInfoAppDetail().get(0);

		versionDTO.setLatestApkVersion(infoAppDetail.getApkVersion());
		versionDTO.setDescription(infoAppDetail.getDescription());

		List<PartnerUrl> partnerUrls = partnerUrlRepository.findAllSortedById();
		if (partnerUrls == null || partnerUrls.isEmpty())
			versionDTO.setPartnerUrls(new ArrayList<>());
		else
			versionDTO.setPartnerUrls(insertPartnerUrlDTOList(partnerUrls));

		return versionDTO;
	}

	private List<PartnerUrlDTO> insertPartnerUrlDTOList(List<PartnerUrl> partnerUrls) {
		List<PartnerUrlDTO> partnerUrlDTOS = new ArrayList<>();

		for (PartnerUrl partnerUrl : partnerUrls) {
			PartnerUrlDTO partnerUrlDTO = new PartnerUrlDTO();

			partnerUrlDTO.setUrlId(partnerUrl.getUrlId());
			partnerUrlDTO.setBaseUrl(partnerUrl.getBaseUrl());
			partnerUrlDTO.setEndpointUrl(partnerUrl.getEndpointUrl());
			partnerUrlDTO.setServiceName(partnerUrl.getServiceName());

			partnerUrlDTOS.add(partnerUrlDTO);
		}
		return partnerUrlDTOS;
	}


	public List<LoginUserDetailDTO> findAllByUserName(String username) {

		List<LoginUserDetail> logList = loginUserDetailRepository.findByUsername(username);
		List<LoginUserDetailDTO> result  = new ArrayList<>();
		for(LoginUserDetail userDetail : logList) {
			LoginUserDetailDTO logDTO = modelMapper.map(userDetail, LoginUserDetailDTO.class);
			result.add(logDTO);
		}

		return result;
	}

	public Map<String, Object> checkData(String login) throws AdiraCustomException{
		Map<String, Object> result =  new HashMap<>();
		String hasil = "";

		try {
			List<LoginUserDetailDTO> cek  = findAllByUserName(login);

			if (!cek.isEmpty()) {
				//update login condition
				updateLogin(cek.get(0).getUsername());
				List<LoginUserDetailDTO> cek2 = findAllByUserName(login);

				result.put(AUTH_FIRST_LOGIN.toString(), cek2.get(0).isFirstTime());
				result.put(AUTH_TOKEN.toString(),"1234567890");
				result.put(AUTH_LOGIN.toString(), login);

			} else {
				//kondisi simpan data username baru
				result.put(AUTH_FIRST_LOGIN.toString(), true);
				result.put(AUTH_TOKEN.toString(),"1234567890");
				result.put(AUTH_LOGIN.toString(), login);


				LoginUserDetail sementara = new LoginUserDetail();
				sementara.setUsername(login);
				sementara.setActiveStatus("A");
				sementara.setLoginCount(1);
				sementara.setFirstTime(true);
				sementara.setStillLogin(true);
				sementara.setLoginCount(1);
				sementara.setCreatedBy(SYSTEM.getMessage());
				sementara.setLastModifiedBy(SYSTEM.getMessage());
				sementara.setLastLoginDate(DateUtil.getCurrentTimestamp());
				loginUserDetailRepository.save(sementara);
			}
		} catch(Exception e) {
			hasil = FAILED_UPDATE_OR_SAVE_DATA_LOGIN.getMessage() + e.getMessage();
			throw new AdiraCustomException(hasil, e);
		}

		return result;
	}

	private void updateLogin(String username){
		List<LoginUserDetail> model = loginUserDetailRepository.findByUsername(username);
		LoginUserDetail entity = model.get(0);
		int loginCount = entity.getLoginCount();
		loginCount += 1;
		entity.setLoginCount(loginCount);
		entity.setFirstTime(false);
		entity.setLastLoginDate(DateUtil.getCurrentTimestamp());

		loginUserDetailRepository.save(entity);
	}

	private void updateLogout(String username){
		List<LoginUserDetail> model = loginUserDetailRepository.findByUsername(username);

		LoginUserDetail entity = model.get(0);
		entity.setStillLogin(false);

		loginUserDetailRepository.save(entity);
	}

	public ResponseDTO getUserPropertyV3(UserDetailRequest userDetailRequest) {

		UserDetail userDetail = userRepository.findUserByUserId(userDetailRequest.getUserId());
		if(userDetail == null)
			return insertUserResponseDtoWhenDataIsNull(USER_NOT_FOUND_WITH_USERNAME.getMessage() + userDetailRequest.getUserId());
		else
			return insertResponseWhenDealerDataExists(userDetail, userDetail.getPartnerCode());

	}

	private String insertOwnerDealerWithAdminDealer(String dlc) {
		UserDetail adminDealer = userRepository.findUserAdminDealerByPartnerCode(dlc);
		if(adminDealer == null)
			return "";
		else
			return adminDealer.getUsername();
	}

	private ResponseDTO insertResponseWhenDealerDataExists(UserDetail ownerDealerUser, String dlc) {
		return new ResponseDTO(1, OK.getMessage(), new Date().toString(), insertToUserDetailDTOV3(ownerDealerUser, dlc));
	}

	private Object insertToUserDetailDTOV3(UserDetail userDetail, String dlc) {
		UserDetailDTOV3 userDetailDTOV3 = modelMapper.map(userDetail, UserDetailDTOV3.class);
		userDetailDTOV3.setPortfolios(insertPortfolio(userDetail));
		userDetailDTOV3.setOwnerDealer(findOwnerOrAdminDealerUsername(dlc));

		return userDetailDTOV3;
	}

	private List<EligibleMenuDTO> insertEligibleMenuForUser(UserDetail userDetail) {
		List<EligibleMenuDTO> eligibleMenuDTOS = new ArrayList<>();
		List<ParaUserDetailMenu> paraUserDetailMenus = paraUserDetailMenuRepository.findAllEligibleMenuForUserByUserId(userDetail.getId());

		for(ParaUserDetailMenu paraUserDetailMenu : paraUserDetailMenus){
			ParaMenuAdmin paraMenuAdmin = paraMenuAdminRepository.findParaMenuAdminBasedOnMenuId(paraUserDetailMenu.getParaMenuAdmin().getParaMenuAdminId());
			if(paraMenuAdmin.getActive() != 1){
				EligibleMenuDTO eligibleMenuDTO = new EligibleMenuDTO(paraMenuAdmin.getParaMenuCode(), paraMenuAdmin.getParaMenuAdminName());
				eligibleMenuDTOS.add(eligibleMenuDTO);
			}
		}
		return eligibleMenuDTOS;
	}

	private String findOwnerOrAdminDealerUsername(String dlc) {
		UserDetail ownerDealerUser = userRepository.findUserOwnerDealerByPartnerCode(dlc);
		if(ownerDealerUser == null)
			return insertOwnerDealerWithAdminDealer(dlc);
		else
			return ownerDealerUser.getUsername();
	}

	private ResponseDTO insertUserResponseDtoWhenDataIsNull(String message) {
		return new ResponseDTO(0, message, new Date().toString(), insertDefaultValueToUserDetailDTOV3());
	}

	private Object insertDefaultValueToUserDetailDTOV3() {
		return new UserDetailDTOV3(0, "", "", "", "", null,
				"", "", "", "", "", "", new Date(),
				"", new Date(), 0, 0, new ArrayList<>(), "" , "", "");
	}

	public ResponseDTO getEligibleMenuByUserId(String userId) {
		ResponseDTO responseDTO = new ResponseDTO();
		try{
			UserDetail userDetail = userRepository.findUserByUserId(userId);
			if(userDetail == null)
				insertDefaultValueToEligibleMenuResponse(USER_NOT_FOUND_WITH_USERNAME.getMessage(), responseDTO);
			else
				mapEligibleMenuForUser(userDetail, responseDTO);
		} catch(Exception e){
			insertErrorResponseWithException(responseDTO, e, ERROR_WHILE_GETTING_ELIGIBLE_MENU_FOR_USER.getMessage());
		}

		return responseDTO;
	}

	private void insertErrorResponseWithException(ResponseDTO responseDTO, Exception e, String errorMessage) {
		responseDTO.setStatus(0);
		responseDTO.setDateTime(new Date().toString());
		responseDTO.setMessage(THERE_IS_SOMETHING_ERROR_IN_OUR_SERVER.getMessage());
		responseDTO.setData(new ArrayList<>());

		LOGGER.error(errorMessage, e.getMessage());
	}

	private void mapEligibleMenuForUser(UserDetail userDetail, ResponseDTO responseDTO) {
		responseDTO.setStatus(0);
		responseDTO.setMessage(OK.getMessage());
		responseDTO.setDateTime(new Date().toString());
		responseDTO.setData(insertEligibleMenuForUser(userDetail));
	}

	private void insertDefaultValueToEligibleMenuResponse(String message, ResponseDTO responseDTO) {
		responseDTO.setStatus(0);
		responseDTO.setMessage(message);
		responseDTO.setDateTime(new Date().toString());
		responseDTO.setData(null);
	}
}
