package id.co.adira.partner.idm.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import id.co.adira.partner.idm.config.BeanConfig;
import id.co.adira.partner.idm.dto.ResponseDTO;
import id.co.adira.partner.idm.dto.UserDetailDTOV3;
import id.co.adira.partner.idm.dto.UserDetailRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.LinkedHashMap;

import static id.co.adira.partner.idm.constant.AppEnum.CONTENT_TYPE;


@Service
public class RestTemplateService {

    private final RestTemplate restTemplate;
    private final Environment env;

    ObjectMapper objectMapper = new BeanConfig().getObjectMapper();

    @Autowired
    public RestTemplateService(RestTemplate restTemplate, Environment env) {
        this.restTemplate = restTemplate;
        this.env = env;
    }

    public String getPartnerId(String email) throws JsonProcessingException {
        UserDetailRequest userDetailRequest = new UserDetailRequest(email);

        HttpHeaders headers = new HttpHeaders();
        headers.set(CONTENT_TYPE.getMessage(), MediaType.APPLICATION_JSON_VALUE);

        HttpEntity entity = new HttpEntity(userDetailRequest, headers);
        UriComponentsBuilder uri =UriComponentsBuilder.fromHttpUrl(env.getProperty("url.api.getUserProperty"));

        String uriToString = uri.toUriString();
        ResponseEntity<String> response = restTemplate.exchange(uri.toUriString(), HttpMethod.POST, entity, String.class);
        ResponseDTO responseDTO = objectMapper.readValue(response.getBody(), ResponseDTO.class);


        LinkedHashMap<String, Object> map = (LinkedHashMap<String, Object>) responseDTO.getData();
        return (String) map.get("partnerCode");

    }
}
