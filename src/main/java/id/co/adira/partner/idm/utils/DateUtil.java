package id.co.adira.partner.idm.utils;

import java.sql.Timestamp;

public class DateUtil {
	
	private DateUtil() {
	    throw new IllegalStateException("Utility class");
	}

	public static Timestamp getCurrentTimestamp() {
		return new Timestamp(System.currentTimeMillis());
	}
}
