package id.co.adira.partner.idm.utils;

import org.springframework.stereotype.Component;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Arrays;

import static id.co.adira.partner.idm.constant.AppEnum.*;

@Component
// This class is used to concatenating Id Prefix with our formatted result
public class FormatUtil {

    public String generateMenuId(Integer menuDataCount){
        String formatResult = formattingForCustomId(menuDataCount);
        return MENU_ID_PREFIX.getMessage() + formatResult;

    }

    public String generateRoleId(Integer roleDataCount){
        String formatResult = formattingForCustomId(roleDataCount);
        return ROLE_ID_PREFIX.getMessage() + formatResult;
    }

    /* This function is used for formatting id before the next data count.
        Example: Number of current data count = 50.
        For the next data, it will be 51. So, the result will be '051'
    */
    private String formattingForCustomId(Integer dataCount){
        NumberFormat numberFormat = new DecimalFormat("000");
        return numberFormat.format(dataCount + 1L);
    }

    public String generateUserCode(Integer userDataCounter){
        String formatResult = formattingForCustomId(userDataCounter);
        return USER_ID_PREFIX.getMessage() + formatResult;
    }
}
