package id.co.adira.partner.idm.utils;

import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class PortfolioMappingUtil {

    public Map<String, String> portfolioMappingFromGetVehicles(){
        Map<String, String> map = new HashMap<>();

        map.put("MOTOR BARU", "001-Motor Baru");
        map.put("MOTOR BEKAS", "002-Motor Bekas");
        map.put("MOBIL BARU", "003-Mobil Baru");
        map.put("MOBIL BEKAS", "004-Mobil Bekas");
        map.put("DURABLE", "005-Durable");

        return map;
    }

    public Map<String, String> portfolioMappingFromTable(){
        Map<String, String> map = new HashMap<>();

        map.put("NMCY", "001-Motor Baru");
        map.put("UMCY", "002-Motor Bekas");
        map.put("NCAR", "003-Mobil Baru");
        map.put("UCAR", "004-Mobil Bekas");;
        map.put("DURABLE", "005-Durable");
        map.put("AXI", "AXI-AXI");
        map.put("KDY", "KDY-Keday");
        map.put("BMM", "BMM-Branch Manager Merchant");
        map.put("SLD", "SLD-Sales Dealer");
        map.put("OWD", "OWD-Owner Dealer");


        return map;
    }
}
