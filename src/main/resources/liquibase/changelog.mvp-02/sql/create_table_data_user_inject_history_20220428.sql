CREATE TABLE if not exists public.tbl_data_user_inject_history (
	branch_code varchar(4) NULL,
	dlc varchar(20) NULL,
	dealer_name varchar(255) NULL,
	product_matrix varchar(100) NULL,
	brand varchar(50) NULL,
	nama_lengkap_sales varchar(100) NULL,
	no_hp varchar(20) NULL,
	email varchar(100) NOT NULL,
	jabatan varchar(100) NULL,
	channel varchar(100) NULL,
	tanggal_upload timestamp NOT NULL DEFAULT now()
);