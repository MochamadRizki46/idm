CREATE TABLE if not exists public.para_menu_admin (
	para_menu_admin_id varchar(36) NOT NULL,
	para_menu_admin_name varchar(100) NOT NULL,
	active numeric(10) NULL,
	created_date timestamp NULL,
	created_by varchar(20) NULL,
	modified_date timestamp NULL,
	modified_by varchar(20) NULL,
	menu_code varchar(10) NULL,
	deleted int4 NULL,
	CONSTRAINT para_menu_admin_pkey PRIMARY KEY (para_menu_admin_id)
);