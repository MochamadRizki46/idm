CREATE TABLE if not exists public.para_partner_type (
	para_partner_type_id varchar(36) NOT NULL,
	partner_type varchar(3) NOT NULL,
	partner_type_name varchar(100) NOT NULL,
	active numeric(10) NULL,
	created_date timestamp NULL,
	created_by varchar(20) NULL,
	modified_date timestamp NULL,
	modified_by varchar(20) NULL,
	deleted int4 NULL,
	CONSTRAINT para_partner_type_pkey PRIMARY KEY (para_partner_type_id)
);