CREATE TABLE if not exists public.para_role (
	role_id bpchar(3) NULL,
	role_name varchar(50) NULL,
	isactive int4 NULL,
	create_by varchar(100) NULL,
	createdate timestamp NULL,
	modify_by varchar(100) NULL,
	modifydate timestamp NULL,
	para_role_id varchar(36) NOT NULL,
	role_code varchar(10) NULL,
	deleted int4 NULL,
	CONSTRAINT para_role_pkey PRIMARY KEY (para_role_id)
);