CREATE TABLE if not exists public.para_role_menu (
	para_role_menu_id varchar(36) NOT NULL,
	para_role_id varchar(36) NOT NULL,
	para_menu_id varchar(36) NOT NULL,
	created_date timestamp NULL,
	created_by varchar(20) NULL,
	modified_date timestamp NULL,
	modified_by varchar(20) NULL,
	active int4 NULL,
	CONSTRAINT para_role_menu_pkey PRIMARY KEY (para_role_menu_id),
	CONSTRAINT fk_para_menu_role_menu FOREIGN KEY (para_menu_id) REFERENCES para_menu_admin(para_menu_admin_id),
	CONSTRAINT fk_para_role_role_menu FOREIGN KEY (para_role_id) REFERENCES para_role(para_role_id)
);