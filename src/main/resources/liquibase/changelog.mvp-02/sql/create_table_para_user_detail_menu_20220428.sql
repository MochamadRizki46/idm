CREATE TABLE if not exists public.para_user_detail_menu (
	para_user_detail_menu_id varchar(36) NOT NULL,
	para_user_detail_id numeric(10) NOT NULL,
	para_menu_admin_id varchar(36) NOT NULL,
	active numeric(10) NULL,
	created_date timestamp NULL,
	created_by varchar(20) NULL,
	modified_date timestamp NULL,
	modified_by varchar(20) NULL,
	CONSTRAINT para_user_detail_user_role_pkey PRIMARY KEY (para_user_detail_menu_id),
	CONSTRAINT fk_para_menu_id FOREIGN KEY (para_menu_admin_id) REFERENCES para_menu_admin(para_menu_admin_id),
	CONSTRAINT fk_para_user_detail FOREIGN KEY (para_user_detail_id) REFERENCES para_user_detail(id)
);