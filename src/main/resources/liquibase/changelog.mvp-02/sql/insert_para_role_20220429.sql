INSERT INTO para_role (role_id,role_name,isactive,create_by,createdate,modify_by,modifydate,para_role_id,role_code,deleted) VALUES 
('OWM','Owner Merchant',0,'SYSTEM','2021-05-31 08:41:45.039','SYSTEM','2022-04-11 09:37:54.238','141c1b32-3897-4ab2-8b86-c6c450aa4afc','002',0)
,('SLD','Sales Dealer',0,'SYSTEM','2021-05-31 08:35:30.755','SYSTEM','2022-04-07 13:37:42.510','5036b65e-b4bd-4b92-be60-f132187a2a45','001',0)
,('AXI','AXI',0,'SYSTEM','2021-05-31 08:41:45.070','SYSTEM','2022-04-08 16:57:29.156','29645126-8df4-4ad0-8090-30c3c014cc09','008',0)
,('BMM','Branch Manager Merchant',0,'SYSTEM','2021-05-31 08:41:45.053','SYSTEM','2022-04-11 09:38:01.522','5e00d5e7-9c86-4a1e-a36e-cc6149cc1100','002',0)
,('BMD','Branch Manager Dealer',0,'SYSTEM','2021-05-31 08:39:11.381','SYSTEM','2022-04-11 09:38:44.554','9f39f5c2-9493-48b1-a6ed-ce2d0e8b5a2b','001',0)
,('SPM','Supervisor Merchant',0,'SYSTEM','2021-05-31 08:41:45.065','SYSTEM','2022-04-11 09:38:07.010','00d7ad5e-7fd9-4d15-9e2e-b744134e61c0','002',0)
,('KDY','Keday',0,'SYSTEM','2021-05-31 08:41:45.074','SYSTEM','2022-04-11 09:38:19.615','8a3dba7c-7fb8-4dcd-b89f-bc4dc17a38b1','009',0)
,('SPD','Supervisor Dealer',0,'SYSTEM','2021-05-31 08:41:45.025','SYSTEM','2022-04-11 09:37:43.694','c04f64ef-6196-437a-a105-6eb8cd06de73','001',0)
,('ADM','Super Administrators',0,'SYSTEM','2021-01-28 15:26:44.877','SYSTEM','2022-04-18 13:49:28.333','004da3ca-dfa3-4cc4-9a32-93d253761ee1','XXX',0)
,('SLM','Sales Merchant',0,'SYSTEM','2021-05-31 08:41:45.035','SYSTEM','2022-04-11 09:37:50.106','73068401-b0dc-4c58-a354-465864b7b40d','002',0)
;
INSERT INTO para_role (role_id,role_name,isactive,create_by,createdate,modify_by,modifydate,para_role_id,role_code,deleted) VALUES 
('OWD','Owner Dealer',0,'SYSTEM','2021-05-31 08:39:11.375','SYSTEM','2022-04-11 09:38:39.314','f5890440-2456-456e-8dd0-b5458bbb5ca2','001',0)
,('ADD','ADMIN DEALER',0,'Admin','2022-04-22 10:24:40.111','SYSTEM','2022-04-22 10:24:40.111','6988217e-8a8b-495b-acee-7a5720c05e81','001',0)
;