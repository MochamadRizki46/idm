alter table public.para_user_detail_menu add created_date timestamp;

alter table public.para_user_detail_menu add created_by varchar(20);

alter table public.para_user_detail_menu add modified_date timestamp;

alter table public.para_user_detail_menu add modified_by varchar(20);