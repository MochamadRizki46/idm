create table public.para_menu_admin(
    para_menu_admin_id varchar(36) primary key not null,
    para_menu_admin_name varchar(100) not null,
    active numeric(10),
    created_date timestamp,
    created_by varchar(20),
    modified_date timestamp,
    modified_by varchar(20)
);