create table public.para_role_menu(
    para_role_menu_id varchar(36) primary key not null,
    para_role_id varchar(36) not null,
    para_menu_id varchar(36) not null,
    created_date timestamp,
    created_by varchar(20),
    modified_date timestamp,
    modified_by varchar(20),

    constraint fk_para_role_role_menu foreign key (para_role_id) references public.para_role(para_role_id),
    constraint fk_para_menu_role_menu foreign key (para_menu_id) references public.para_menu_admin(para_menu_admin_id)
);