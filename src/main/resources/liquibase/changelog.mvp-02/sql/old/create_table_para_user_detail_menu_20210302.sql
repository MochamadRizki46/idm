create table para_user_detail_user_role(
    para_user_detail_role_id varchar(36) primary key not null ,
    para_user_detail_id numeric(10) not null,
    para_menu_admin_id varchar(36) not null,
    active numeric(10),

    constraint fk_para_user_detail foreign key (para_user_detail_id) references public.para_user_detail(id),
    constraint fk_para_menu_id foreign key (para_menu_admin_id) references public.para_menu_admin(para_menu_admin_id)

);