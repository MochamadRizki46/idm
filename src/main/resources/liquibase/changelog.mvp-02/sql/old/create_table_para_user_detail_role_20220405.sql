create table public.para_user_detail_role(
    para_user_detail_role_id varchar(36) primary key not null,
    para_user_detail_id integer not null,
    para_user_role_id varchar(36),

    constraint fk_para_user_detail foreign key (para_user_detail_id) references public.para_user_detail(id),
    constraint fk_para_role foreign key (para_user_role_id) references public.para_role(para_role_id)
);