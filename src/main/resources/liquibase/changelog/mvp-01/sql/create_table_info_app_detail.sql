CREATE TABLE public.tbl_info_app_detail(
  url_detail_id int primary key not null,
  apk_version varchar(20) not null,
  description varchar(255),
  created_date timestamp,
  modified_date timestamp

);