CREATE TABLE public.login_user_detail (
	id int4 NOT NULL,
	created_by varchar(50) NOT NULL,
	created_date timestamp NOT NULL,
	last_modified_by varchar(50) NULL,
	last_modified_date timestamp NULL,
	active_status varchar(255) NULL,
	is_first_time bool NULL,
	is_still_login bool NULL,
	last_login_date timestamp NULL,
	login_count int4 NULL,
	username varchar(255) NULL,
	CONSTRAINT login_user_detail_pkey PRIMARY KEY (id)
);

CREATE SEQUENCE public.lud_seq_gen
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 1
	CACHE 1
	NO CYCLE;