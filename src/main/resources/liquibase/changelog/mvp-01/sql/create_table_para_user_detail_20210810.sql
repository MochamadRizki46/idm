CREATE TABLE public.para_user_detail (
	id numeric(10) NOT NULL, -- id
	usertype bpchar(3) NULL, -- tipe user partner
	userid varchar(100) NULL, -- id user
	roleid bpchar(3) NULL, -- tipe user partner
	placeofbirth varchar(50) NULL, -- tempat lahir
	birthday date NULL, -- tanggal lahir
	email varchar(100) NULL, -- alamat email
	handphone varchar(20) NULL, -- nomor handphone
	wa varchar(20) NULL, -- nomor wa
	partner_code varchar(16) NULL, -- kode partner
	partner_name varchar(100) NULL, -- nama partner
	create_by varchar(100) NULL,
	createdate timestamp NULL,
	modify_by varchar(100) NULL,
	modifydate timestamp NULL,
	isactive int4 NULL, -- active = 0, non active = 1
	deleted int4 NULL, -- true = 1, false = 0
	portfolio varchar(50) NULL, -- portfolio
	brand varchar(50) NULL, -- merk jualan partner
	username varchar(100) NULL, -- nama user
	CONSTRAINT para_user_detail_pk PRIMARY KEY (id)
);

-- Column comments

COMMENT ON COLUMN public.para_user_detail.id IS 'id';
COMMENT ON COLUMN public.para_user_detail.usertype IS 'tipe user partner';
COMMENT ON COLUMN public.para_user_detail.userid IS 'id user';
COMMENT ON COLUMN public.para_user_detail.roleid IS 'tipe user partner';
COMMENT ON COLUMN public.para_user_detail.placeofbirth IS 'tempat lahir';
COMMENT ON COLUMN public.para_user_detail.birthday IS 'tanggal lahir';
COMMENT ON COLUMN public.para_user_detail.email IS 'alamat email';
COMMENT ON COLUMN public.para_user_detail.handphone IS 'nomor handphone';
COMMENT ON COLUMN public.para_user_detail.wa IS 'nomor wa';
COMMENT ON COLUMN public.para_user_detail.partner_code IS 'kode partner';
COMMENT ON COLUMN public.para_user_detail.partner_name IS 'nama partner';
COMMENT ON COLUMN public.para_user_detail.isactive IS 'active = 0, non active = 1';
COMMENT ON COLUMN public.para_user_detail.deleted IS 'true = 1, false = 0';
COMMENT ON COLUMN public.para_user_detail.portfolio IS 'portfolio';
COMMENT ON COLUMN public.para_user_detail.brand IS 'merk jualan partner';
COMMENT ON COLUMN public.para_user_detail.username IS 'nama user';