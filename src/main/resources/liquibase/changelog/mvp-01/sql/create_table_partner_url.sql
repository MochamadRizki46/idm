CREATE TABLE public.tbl_list_url_partner(
    url_id int primary key not null,
    service_name varchar(100) not null,
    base_url varchar(255),
    endpoint_url varchar(255)

);

COMMENT ON COLUMN public.tbl_list_url_partner.url_id IS 'URL Id';
COMMENT ON COLUMN public.tbl_list_url_partner.service_name IS 'Name of the service where URL is located';
COMMENT ON COLUMN public.tbl_list_url_partner.base_url IS 'Base URL that used on Ad1partner mobile';
COMMENT ON COLUMN public.tbl_list_url_partner.endpoint_url IS 'Endpoint url that used on Ad1partner mobile';

