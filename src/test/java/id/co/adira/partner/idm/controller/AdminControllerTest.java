package id.co.adira.partner.idm.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import id.co.adira.partner.idm.dto.*;
import id.co.adira.partner.idm.entity.RoleDetail;
import id.co.adira.partner.idm.service.AdminService;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static id.co.adira.partner.idm.constant.AppEnum.OK;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
public class AdminControllerTest {

    @InjectMocks
    AdminController adminController;

    @MockBean
    AdminService adminService;

    @Autowired
    MockMvc mockMvc;

    @Autowired
    WebApplicationContext context;

    ObjectMapper objectMapper = new ObjectMapper();

    @BeforeEach
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
    }

    @Test
    public void testGetAllPicAndExpectStatusOk() throws Exception {
        FilterPageAndDataDTO filterPageAndDataDTO = new FilterPageAndDataDTO();
        UserDetailsResponseDTO firstUserDetail = new UserDetailsResponseDTO();
        UserDetailsResponseDTO secondUserDetail = new UserDetailsResponseDTO();

        List<UserDetailsResponseDTO> userDetails = new ArrayList<>();


        filterPageAndDataDTO.setPage(1);
        filterPageAndDataDTO.setDataCount(2);

        firstUserDetail.setUserId("test@ad1gate.com");
        firstUserDetail.setEmail("test@ad1gate.com");
        firstUserDetail.setUsername("Test User");
        firstUserDetail.setPartnerName("dummy_partner_name");
        firstUserDetail.setPartnerId("PRT001");
        firstUserDetail.setHandphoneNo("081234567890");
        firstUserDetail.setUserType("dummy_user_type");

        userDetails.add(firstUserDetail);

        secondUserDetail.setUserId("test2@ad1gate.com");
        secondUserDetail.setEmail("test2@ad1gate.com");
        secondUserDetail.setUsername("Test User 2");
        secondUserDetail.setPartnerName("dummy_partner_name");
        secondUserDetail.setPartnerId("PRT002");
        secondUserDetail.setHandphoneNo("081234567890");
        secondUserDetail.setUserType("dummy_user_type");

        userDetails.add(secondUserDetail);

        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setHttpStatus(HttpStatus.OK);
        baseResponse.setStatus(0);
        baseResponse.setMessage(OK.getMessage());
        baseResponse.setData(userDetails);

        String jsonStr = objectMapper.writeValueAsString(filterPageAndDataDTO);

        when(adminService.showAllPic(any(FilterPageAndDataDTO.class))).thenReturn(baseResponse);
        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/admin/show-all-pic")
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).content(jsonStr);

        mockMvc.perform(requestBuilder).andExpect(status().isOk());
    }

    @Test
    public void testShowAllMenuAndExpectStatusOk() throws Exception {
        FilterPageAndDataDTO filterPageAndDataDTO = new FilterPageAndDataDTO();
        filterPageAndDataDTO.setPage(1);
        filterPageAndDataDTO.setDataCount(2);

        MenuAdminHeadResponseDTO menuAdminHeadResponseDTO = new MenuAdminHeadResponseDTO();
        List<MenuAdminDetailResponseDTO> menuAdmins = new ArrayList<>();

        MenuAdminDetailResponseDTO firstMenuAdmin = new MenuAdminDetailResponseDTO();
        firstMenuAdmin.setMenuId("MNU001");
        firstMenuAdmin.setMenuName("test_menu_name");
        firstMenuAdmin.setActiveStatus(0);

        menuAdmins.add(firstMenuAdmin);

        MenuAdminDetailResponseDTO secondMenuAdmin = new MenuAdminDetailResponseDTO();
        secondMenuAdmin.setMenuId("MNU001");
        secondMenuAdmin.setMenuName("test_menu_name");
        secondMenuAdmin.setActiveStatus(0);

        menuAdmins.add(secondMenuAdmin);

        menuAdminHeadResponseDTO.setMenuList(menuAdmins);

        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setHttpStatus(HttpStatus.OK);
        baseResponse.setStatus(0);
        baseResponse.setMessage(OK.getMessage());
        baseResponse.setData(menuAdminHeadResponseDTO);

        when(adminService.showAllMenu(any(FilterPageAndDataDTO.class))).thenReturn(baseResponse);
        String jsonStr = objectMapper.writeValueAsString(filterPageAndDataDTO);

        when(adminService.showAllPic(any(FilterPageAndDataDTO.class))).thenReturn(baseResponse);
        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/admin/show-all-menu")
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).content(jsonStr);

        mockMvc.perform(requestBuilder).andExpect(status().isOk());

    }

    @Test
    public void testShowAllRoleAndExpectStatusOk() throws Exception {
        FilterPageAndDataDTO filterPageAndDataDTO = new FilterPageAndDataDTO();
        filterPageAndDataDTO.setPage(1);
        filterPageAndDataDTO.setDataCount(2);

        RoleHeadResponseDTO roleHeadResponseDTO = new RoleHeadResponseDTO();
        List<AllRoleResponseDTO> roleDetails = new ArrayList<>();

        AllRoleResponseDTO firstRoleDetail = new AllRoleResponseDTO();
        firstRoleDetail.setPartnerType("001");
        firstRoleDetail.setRoleName("first_dummy_role_name");
        firstRoleDetail.setActive(0);

        roleDetails.add(firstRoleDetail);

        AllRoleResponseDTO secondRoleDetail = new AllRoleResponseDTO();
        secondRoleDetail.setPartnerType("002");
        secondRoleDetail.setRoleName("second_dummy_role_name");
        secondRoleDetail.setActive(0);

        roleDetails.add(secondRoleDetail);
        roleHeadResponseDTO.setRoles(roleDetails);

        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setHttpStatus(HttpStatus.OK);
        baseResponse.setMessage(OK.getMessage());
        baseResponse.setStatus(0);
        baseResponse.setData(roleHeadResponseDTO);

        when(adminService.showAllRole(any(FilterPageAndDataDTO.class))).thenReturn(baseResponse);

        String jsonStr = objectMapper.writeValueAsString(filterPageAndDataDTO);
        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/admin/show-all-role")
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).content(jsonStr);

        mockMvc.perform(requestBuilder).andExpect(status().isOk());

    }

    @Test
    public void testGenerateMenuIdAndExpectStatusOk() throws Exception {
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setHttpStatus(HttpStatus.OK);
        baseResponse.setStatus(0);
        baseResponse.setMessage(OK.getMessage());
        baseResponse.setData("MNU001");

        when(adminService.generateMenuId()).thenReturn(baseResponse);
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/admin/generate-menu-id")
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON);

        mockMvc.perform(requestBuilder).andExpect(status().isOk());

    }

    @Test
    public void testGenerateRoleIdAndExpectStatusOk() throws Exception {
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setHttpStatus(HttpStatus.OK);
        baseResponse.setStatus(0);
        baseResponse.setMessage(OK.getMessage());
        baseResponse.setData("ROL001");

        when(adminService.generateRoleId()).thenReturn(baseResponse);
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/admin/generate-role-id")
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON);

        mockMvc.perform(requestBuilder).andExpect(status().isOk());
    }

    @Test
    public void testInsertOrUpdateRoleAndExpectStatusOk() throws Exception {
        RoleRequestDTO roleRequestDTO = new RoleRequestDTO();
        roleRequestDTO.setRoleId("AAA");
        roleRequestDTO.setPartnerType("CCC");
        roleRequestDTO.setRoleName("dummy_role_name");

        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setHttpStatus(HttpStatus.OK);
        baseResponse.setStatus(0);
        baseResponse.setMessage(OK.getMessage());

        RoleDetail roleDetail = new RoleDetail();
        roleDetail.setRoleId(roleRequestDTO.getRoleId());
        roleDetail.setRoleName(roleRequestDTO.getRoleName());
        roleDetail.setIsActive(0);
        roleDetail.setCreateDate(new Date());
        roleDetail.setModifyDate(new Date());
        roleDetail.setModifyBy("Admin");
        roleDetail.setCreateBy("Admin");

        baseResponse.setData(roleDetail);
        when(adminService.insertOrUpdateRole(any(RoleRequestDTO.class))).thenReturn(baseResponse);

        String jsonStr = objectMapper.writeValueAsString(roleRequestDTO);
        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/admin/update-role")
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).content(jsonStr);

        mockMvc.perform(requestBuilder).andExpect(status().isOk());

    }

    @Test
    public void testInsertOrUpdatePicAndExpectStatusOk() throws Exception {
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setHttpStatus(HttpStatus.OK);
        baseResponse.setStatus(0);
        baseResponse.setMessage(OK.getMessage());

        List<String> checkedMenus = new ArrayList<>();
        checkedMenus.add("MNU001");
        checkedMenus.add("MNU002");

        List<String> uncheckedMenus = new ArrayList<>();
        uncheckedMenus.add("MNU003");

        PicRequestDTO picRequestDTO = new PicRequestDTO();
        picRequestDTO.setUserId("test@ad1gate.com");
        picRequestDTO.setHandphoneNo("081234567890");
        picRequestDTO.setPartnerCode("dummy_partner_code");
        picRequestDTO.setPartnerName("dummy_partner_name");
        picRequestDTO.setBirthDate(String.valueOf(new Date()));
        picRequestDTO.setBirthPlace("dummy_birth_place");
        picRequestDTO.setNoWa("081234567890");
        picRequestDTO.setUserType("dummy_user_type");
        picRequestDTO.setCheckedMenuCodes(checkedMenus);

        when(adminService.insertOrUpdatePic(any(PicRequestDTO.class))).thenReturn(baseResponse);
        String jsonStr = objectMapper.writeValueAsString(picRequestDTO);
        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/admin/update-pic")
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).content(jsonStr);

        mockMvc.perform(requestBuilder).andExpect(status().isOk());

    }

    @Test
    public void testInsertOrUpdateMenuAndExpectStatusOk() throws Exception {
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setHttpStatus(HttpStatus.OK);
        baseResponse.setStatus(0);
        baseResponse.setMessage(OK.getMessage());

        AdminMenuRequestDTO adminMenuRequestDTO = new AdminMenuRequestDTO();
        adminMenuRequestDTO.setMenuCode("MNU001");
        adminMenuRequestDTO.setMenuName("dummy_menu_name");

        when(adminService.insertOrUpdateMenu(any(AdminMenuRequestDTO.class))).thenReturn(baseResponse);
        String jsonStr = objectMapper.writeValueAsString(adminMenuRequestDTO);
        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/admin/insert-or-update-menu")
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).content(jsonStr);

        mockMvc.perform(requestBuilder).andExpect(status().isOk());

    }

    @Test
    public void testUpdateStatusPicAndExpectStatusOk(){
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setHttpStatus(HttpStatus.OK);
        baseResponse.setStatus(0);
        baseResponse.setMessage(OK.getMessage());

    }

}
