package id.co.adira.partner.idm.controller;

import id.co.adira.partner.idm.dto.PartnerUrlDTO;
import id.co.adira.partner.idm.dto.VersionDTO;
import id.co.adira.partner.idm.service.IDMService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@SpringBootTest
@ActiveProfiles("test")
public class PartnerUrlControllerTest {

    @InjectMocks
    PartnerUrlController partnerUrlController;

    @Mock
    IDMService idmService;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);

        List<PartnerUrlDTO> partnerUrlDTOS = new ArrayList<>();

        PartnerUrlDTO firstPartnerUrlDTO = new PartnerUrlDTO();
        firstPartnerUrlDTO.setUrlId(1);
        firstPartnerUrlDTO.setBaseUrl("http://localhost:8080/");
        firstPartnerUrlDTO.setServiceName("first_service");
        firstPartnerUrlDTO.setEndpointUrl("/test/sample/first/url");

        partnerUrlDTOS.add(firstPartnerUrlDTO);

        PartnerUrlDTO secondPartnerUrlDTO = new PartnerUrlDTO();
        secondPartnerUrlDTO.setUrlId(2);
        secondPartnerUrlDTO.setBaseUrl("http://localhost:8080");
        secondPartnerUrlDTO.setServiceName("second_service");
        secondPartnerUrlDTO.setEndpointUrl("/test/sample/second/url");

        partnerUrlDTOS.add(secondPartnerUrlDTO);

        VersionDTO versionDTO = new VersionDTO();
        versionDTO.setLatestApkVersion("v1.0.3");
        versionDTO.setDescription("This is latest APK version");
        versionDTO.setPartnerUrls(partnerUrlDTOS);

        when(idmService.getInfoApps()).thenReturn(versionDTO);
    }

    @Test
    public void getAllUrlPartnerTest(){
        VersionDTO response = partnerUrlController.getInfoApps();
        assertTrue(response.getPartnerUrls().size() == 2);
    }


}
