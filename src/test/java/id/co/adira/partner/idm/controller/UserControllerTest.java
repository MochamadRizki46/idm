package id.co.adira.partner.idm.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import id.co.adira.partner.idm.dto.*;
import id.co.adira.partner.idm.entity.UserDetail;
import id.co.adira.partner.idm.repository.UserRepository;
import id.co.adira.partner.idm.service.IDMService;
import io.swagger.models.Response;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.List;

import static id.co.adira.partner.idm.constant.AppEnum.OK;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureMockMvc
public class UserControllerTest {

	@InjectMocks
	UserController userController;

	@Mock
	IDMService idmService;

	@Mock
	UserRepository userRepository;

	@Autowired
	MockMvc mockMvc;

	@Autowired
	WebApplicationContext context;

	ObjectMapper objectMapper = new ObjectMapper();

	@BeforeEach
	public void setUp(){
		mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
		MockitoAnnotations.initMocks(this);
	}


	@Test
	public void testAuthUserAndExpectStatusOk() throws Exception {
		StandardResponseDTO standardResponseDTO = new StandardResponseDTO();
		standardResponseDTO.setStatus(1);
		standardResponseDTO.setMessage("Success");

		UserLoginRequest userLoginRequest = new UserLoginRequest();
		userLoginRequest.setLogin("test@ad1gate.com");
		userLoginRequest.setPassword("test123");

		UserDetailDTO userDetailDTO = new UserDetailDTO();
		userDetailDTO.setUserId("US001");
		userDetailDTO.setUserType("dummy_user_type");
		userDetailDTO.setPartnerCode("PRT001");
		userDetailDTO.setPortfolio("UMCY");
		userDetailDTO.setBrand("Hodna");

		when(idmService.authUser(userLoginRequest)).thenReturn(standardResponseDTO);
		when(idmService.findUserByIdV1("US001")).thenReturn(userDetailDTO);

		String jsonStr = objectMapper.writeValueAsString(userLoginRequest);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/users/authUser")
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).content(jsonStr);

		mockMvc.perform(requestBuilder).andExpect(status().isOk());
	}

	@Test
	public void testGetUserPropertyAndExpectStatusOk() throws Exception {
		UserDetailRequest userDetailRequest = new UserDetailRequest();
		userDetailRequest.setUserId("US001");

		UserDetailDTO userDetailDTO = new UserDetailDTO();
		userDetailDTO.setUserId("US001");
		userDetailDTO.setUserType("dummy_user_type");
		userDetailDTO.setPartnerCode("PRT001");
		userDetailDTO.setPortfolio("UMCY");
		userDetailDTO.setBrand("Hodna");

		when(idmService.findUserByIdV1("US001")).thenReturn(userDetailDTO);
		String jsonStr = objectMapper.writeValueAsString(userDetailRequest);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/users/getUserProperty")
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).content(jsonStr);

		mockMvc.perform(requestBuilder).andExpect(status().isOk());
	}

	@Test
	public void testAuthUserV2AndExpectStatusOk() throws Exception {

		UserLoginRequest userLoginRequest = new UserLoginRequest();
		userLoginRequest.setLogin("test@ad1gate.com");
		userLoginRequest.setPassword("test123");

		UserDetailRequest userDetailRequest = new UserDetailRequest();
		userDetailRequest.setUserId("test@ad1gate.com");

		StandardResponseDTO standardResponseDTO = new StandardResponseDTO();
		standardResponseDTO.setStatus(1);
		standardResponseDTO.setMessage("Success");

		List<PortfolioDTO> portfolioDTOS = new ArrayList<>();

		PortfolioDTO portfolioDTO = new PortfolioDTO("OBJ001", "dummy_object_desc");
		portfolioDTOS.add(portfolioDTO);

		UserDetailDTOV3 userDetailDTOV3 = new UserDetailDTOV3();
		userDetailDTOV3.setUserId("US001");
		userDetailDTOV3.setUserType("dummy_user_type");
		userDetailDTOV3.setPartnerCode("PRT001");
		userDetailDTOV3.setPortfolios(portfolioDTOS);
		userDetailDTOV3.setBrand("Hodna");
		userDetailDTOV3.setOwnerDealer("dummy_owner_dealer");

		ResponseDTO responseDTO = new ResponseDTO(1, OK.getMessage(), userDetailDTOV3);

		when(idmService.authUser(userLoginRequest)).thenReturn(standardResponseDTO);
		when(idmService.getUserPropertyV3(userDetailRequest)).thenReturn(responseDTO);
		String jsonStr = objectMapper.writeValueAsString(userLoginRequest);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/users/v2/authUser")
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).content(jsonStr);

		mockMvc.perform(requestBuilder).andExpect(status().isOk());
	}

	@Test
	public void testUpdateProfileAndExpectSuccessful() throws Exception {
		UpdateProfileDTO updateProfileDTO = new UpdateProfileDTO();
		updateProfileDTO.setUserId("US001");
		updateProfileDTO.setEmail("test@ad1gate.com");
		updateProfileDTO.setUserName("dummy_username");
		updateProfileDTO.setPhoneNumber("01234567890");
		updateProfileDTO.setWaNumber("081234567890");

		when(idmService.updateProfile(updateProfileDTO)).thenReturn("Update success");
		String jsonStr = objectMapper.writeValueAsString(updateProfileDTO);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/users/updateProfile")
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).content(jsonStr);

		mockMvc.perform(requestBuilder).andExpect(status().isOk());

	}

	@Test
	public void testChangePasswordAndExpectSuccessful() throws Exception {
		ChangePasswordRequest changePasswordRequest = new ChangePasswordRequest();
		changePasswordRequest.setLogin("test@ad1gate.com");
		changePasswordRequest.setPassword("test123");
		changePasswordRequest.setNewpassword("test1234");

		StandardResponseDTO standardResponseDTO = new StandardResponseDTO();
		standardResponseDTO.setStatus(1);
		standardResponseDTO.setMessage("Success");

		when(idmService.changePassword(changePasswordRequest)).thenReturn(standardResponseDTO);
		String jsonStr = objectMapper.writeValueAsString(changePasswordRequest);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/users/changePassword")
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).content(jsonStr);

		mockMvc.perform(requestBuilder).andExpect(status().isOk());
	}

	@Test
	public void testResetPasswordAndExpectStatusOk() throws Exception {
		ResetPasswordRequest resetPasswordRequest = new ResetPasswordRequest();
		resetPasswordRequest.setLogin("test@ad1gate.com");
		resetPasswordRequest.setNewpassword("test1234");

		StandardResponseDTO standardResponseDTO = new StandardResponseDTO();
		standardResponseDTO.setStatus(1);
		standardResponseDTO.setMessage("Success");

		when(idmService.resetPassword(resetPasswordRequest)).thenReturn(standardResponseDTO);
		String jsonStr = objectMapper.writeValueAsString(resetPasswordRequest);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/users/resetPassword")
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).content(jsonStr);

		mockMvc.perform(requestBuilder).andExpect(status().isOk());
	}
}
