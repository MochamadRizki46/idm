package id.co.adira.partner.idm.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import id.co.adira.partner.idm.dto.*;
import id.co.adira.partner.idm.entity.InfoAppDetail;
import id.co.adira.partner.idm.entity.LoginUserDetail;
import id.co.adira.partner.idm.entity.PartnerUrl;
import id.co.adira.partner.idm.entity.UserDetail;
import id.co.adira.partner.idm.exception.AdiraCustomException;
import id.co.adira.partner.idm.repository.InfoAppDetailRepository;
import id.co.adira.partner.idm.repository.LoginUserDetailRepository;
import id.co.adira.partner.idm.repository.PartnerUrlRepository;
import id.co.adira.partner.idm.repository.UserRepository;
import id.co.adira.partner.idm.utils.PortfolioMappingUtil;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.sql.Timestamp;
import java.util.*;

import static id.co.adira.partner.idm.constant.AppEnum.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;


@SpringBootTest
public class IDMServiceTest {

    @InjectMocks
    IDMService idmService;

    @Mock
    UserRepository userRepository;

    @Mock
    PartnerUrlRepository partnerUrlRepository;

    @Mock
    PortfolioMappingUtil portfolioMappingUtil;

    @Mock
    InfoAppDetailRepository infoAppDetailRepository;

    @Mock
    LoginUserDetailRepository loginUserDetailRepository;

    UserDetail userDetail = new UserDetail();

    UserDetail userDetailV2 = new UserDetail();

    @Mock
    ModelMapper modelMapper;

    UserDetailDTO userDetailDTO = new UserDetailDTO();

    UserDetailDTOV2 userDetailDTOV2 = new UserDetailDTOV2();

    UpdateProfileDTO updateProfileDTO = new UpdateProfileDTO();

    UserLoginRequest userLoginRequest = new UserLoginRequest();

    ResponseDTO responseDTO = new ResponseDTO();

    @Mock
    RestTemplate restTemplate;

    @Mock
    ResponseEntity<String> responseEntity;

    @Mock
    ObjectMapper objectMapper;

    StandardResponseDTO standardResponseDTO = new StandardResponseDTO();

    ChangePasswordRequest changePasswordRequest = new ChangePasswordRequest();

    ResetPasswordRequest resetPasswordRequest = new ResetPasswordRequest();

    Map<String, String> map = new HashMap<>();

    HttpHeaders headers = new HttpHeaders();


    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);

        userDetail.setUserId("US001");
        userDetail.setUserType("type_1");
        userDetail.setDeleted(0);
        userDetail.setIsactive(1);
        userDetail.setPartnerCode("PA001");
        userDetail.setNoWa("01234567890");
        userDetail.setHandphone("01234567890");
        userDetail.setCreateBy("Me");
        userDetail.setId(1);
        userDetail.setEmail("sample@adira.com");

        userDetailV2.setUserId("US002");
        userDetailV2.setUserType("type_2");
        userDetailV2.setDeleted(0);
        userDetailV2.setIsactive(1);
        userDetailV2.setPartnerCode("PA002");
        userDetailV2.setNoWa("081234567890");
        userDetailV2.setHandphone("081234567890");
        userDetailV2.setCreateBy("Me");
        userDetailV2.setId(1);
        userDetailV2.setEmail("sample@adira.com");

        userDetailDTO.setUserId("US001");
        userDetailDTO.setUserType("type_1");
        userDetailDTO.setDeleted(0);
        userDetailDTO.setIsactive(1);
        userDetailDTO.setPartnerCode("PA001");
        userDetailDTO.setNoWa("01234567890");
        userDetailDTO.setHandphone("01234567890");
        userDetailDTO.setCreateBy("Me");
        userDetailDTO.setId(1);
        userDetailDTO.setEmail("sample@adira.com");

        updateProfileDTO.setEmail("sample2@adira.com");
        updateProfileDTO.setUserName("second_username");
        updateProfileDTO.setPhoneNumber("081234556789");
        updateProfileDTO.setUserId("US001");

        userLoginRequest.setLogin("US001");
        userLoginRequest.setPassword("test123");

        standardResponseDTO.setStatus(0);
        standardResponseDTO.setMessage(OK.getMessage());

        changePasswordRequest.setLogin("US001");
        changePasswordRequest.setPassword("test123");
        changePasswordRequest.setNewpassword("test1234");

        resetPasswordRequest.setLogin("US001");
        resetPasswordRequest.setNewpassword("new_pass");

        responseDTO.setStatus(0);
        responseDTO.setMessage("Success");
        responseDTO.setData(mapDataResponseToString());

        map.put("NMCY", "001-Motor Baru");
        map.put("UMCY", "002-Motor Bekas");
        map.put("NCAR", "003-Mobil Baru");
        map.put("UCAR", "004-Mobil Bekas");;
        map.put("AXI", "005-AXI");
        map.put("KDY", "006-Keday");

        String urlLogin = "https://af-extwsuat.adira.co.id/CommonsWebAPI/api/PublicAccess/Authenticate";
        String urlResetPassword = "https://af-extwsuat.adira.co.id/CommonsWebAPI/api/PublicAccess/ForgotPassword";
        String urlChangePassword = "https://af-extwsuat.adira.co.id/CommonsWebAPI/api/PublicAccess/ChangePassword";
        String urlGetProperty = "https://af-extwsuat.adira.co.id/Ad1gateAPI/api/Submit/GetVehicleType";

        ReflectionTestUtils.setField(idmService,"urlAuth",urlLogin);
        ReflectionTestUtils.setField(idmService, "urlChangePassword", urlChangePassword);
        ReflectionTestUtils.setField(idmService, "urlResetPassword", urlResetPassword);
        ReflectionTestUtils.setField(idmService, "urlGetProperty", urlGetProperty);

        headers.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        HttpEntity httpEntity = new HttpEntity(headers);

        UriComponentsBuilder uri = UriComponentsBuilder.fromHttpUrl(urlGetProperty)
                .queryParam("branchCode", "0000")
                .queryParam("dealerCode", userDetailV2.getPartnerCode());

        when(restTemplate.postForEntity(
                urlChangePassword,
                changePasswordRequest,
                String.class)).
                thenReturn(responseEntity);

        when(restTemplate.postForEntity(
                urlLogin,
                userLoginRequest,
                String.class)).
                thenReturn(responseEntity);


        when(restTemplate.postForEntity(
                urlResetPassword,
                resetPasswordRequest,
                String.class)).
                thenReturn(responseEntity);

        ResponseEntity<String> responseEntity1 = new ResponseEntity<String>(mapJsonResponseToString(), HttpStatus.OK);

        when(restTemplate.exchange(
                uri.toUriString(),
                HttpMethod.GET,
                httpEntity,
                String.class)).
                thenReturn(responseEntity1);

    }

    @Test
    public void findUserByIdAndExpectReturnUserDetail() throws AdiraCustomException, JsonProcessingException {
        when(userRepository.findUserByUserId("US001")).thenReturn(userDetail);
        when(modelMapper.map(userDetail, UserDetailDTO.class)).thenReturn(userDetailDTO);

        UserDetailDTO response = idmService.findUserByIdV1("US001");
        assertEquals("US001", response.getUserId());

    }

    @Test(expected = AdiraCustomException.class)
    public void findUserByIdAndExpectThrowAnException() throws AdiraCustomException {
        when(userRepository.findUserByUserId("US001")).thenReturn(null);
        idmService.findUserByIdV1("US001");

    }

    @Test
    public void updateUserProfileAndExpectUpdateSuccess() throws AdiraCustomException {
        when(userRepository.findUserByUserId("US001")).thenReturn(userDetail);
        UserDetail currentUserDetail = new UserDetail();

        currentUserDetail.setUsername(updateProfileDTO.getUserName());
        currentUserDetail.setEmail(updateProfileDTO.getEmail());
        currentUserDetail.setUserId(updateProfileDTO.getUserId());
        currentUserDetail.setHandphone(updateProfileDTO.getPhoneNumber());
        currentUserDetail.setNoWa(updateProfileDTO.getWaNumber());

        when(userRepository.save(currentUserDetail)).thenReturn(any());

        String response = idmService.updateProfile(updateProfileDTO);
        assertEquals("Update Success", response);
    }

    @Test
    public void updateProfileTestAndExpectUserNotFound() throws AdiraCustomException {
        when(userRepository.findUserByUserId("US001")).thenReturn(null);

        String response = idmService.updateProfile(updateProfileDTO);
        assertEquals("Data Not Found", response);
    }

    @Test(expected = AdiraCustomException.class)
    public void updateProfileTestAndExpectThrowAnException() throws AdiraCustomException {
        given(userRepository.findUserByUserId("US001")).willAnswer(invocation -> {
        throw new AdiraCustomException("Gagal Update Profile null");
        });

        idmService.updateProfile(updateProfileDTO);
    }

    @Test
    public void authServiceTestAndExpectReturnResponse() throws JsonProcessingException, AdiraCustomException, JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("Status", 0);
        jsonObject.put("Message", OK.getMessage());

        String json = jsonObject.toString();

        when(objectMapper.readValue(json, StandardResponseDTO.class)).thenReturn(standardResponseDTO);

        StandardResponseDTO response = idmService.authUser(userLoginRequest);
        assertEquals(response.getStatus(), 0);

    }

    @Test(expected = AdiraCustomException.class)
    public void authServiceTestAndExpectThrowAnError() throws AdiraCustomException {
        String data = "abc@adira.com";
        ReflectionTestUtils.setField(idmService, "urlAuth", data);

        when(restTemplate.postForEntity(data, userLoginRequest, String.class)).thenAnswer(invocationOnMock -> AdiraCustomException.class);
        idmService.authUser(userLoginRequest);
    }

    @Test
    public void changePasswordTestAndExpectReturnAResponse() throws JSONException, JsonProcessingException, AdiraCustomException {

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("Status", 0);
        jsonObject.put("Message", OK.getMessage());

        String json = jsonObject.toString();

        when(objectMapper.readValue(json, StandardResponseDTO.class)).thenReturn(standardResponseDTO);

        StandardResponseDTO response = idmService.changePassword(changePasswordRequest);
        assertEquals(response.getStatus(), 0);
    }

    @Test
    public void resetPasswordTestAndExpectReturnResponse() throws JSONException, JsonProcessingException, AdiraCustomException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("Status", 0);
        jsonObject.put("Message", OK.getMessage());

        String json = jsonObject.toString();

        when(objectMapper.readValue(json, StandardResponseDTO.class)).thenReturn(standardResponseDTO);

        StandardResponseDTO response = idmService.resetPassword(resetPasswordRequest);
        assertEquals(response.getStatus(), 0);
    }

    @Test(expected = AdiraCustomException.class)
    public void resetPasswordTestAndExpectThrowAnException() throws AdiraCustomException {
        String data = "abc@adira.com";
        ReflectionTestUtils.setField(idmService, "urlResetPassword", data);

        when(restTemplate.postForEntity(data, resetPasswordRequest, String.class)).thenAnswer(invocationOnMock -> AdiraCustomException.class);
        idmService.resetPassword(resetPasswordRequest);
    }

    @Test(expected = AdiraCustomException.class)
    public void changePasswordTestAndExpectThrowAnException() throws AdiraCustomException {
        String data = "abc@adira.com";
        ReflectionTestUtils.setField(idmService, "urlChangePassword", data);

        when(restTemplate.postForEntity(data, changePasswordRequest, String.class)).thenAnswer(invocationOnMock -> AdiraCustomException.class);
        idmService.changePassword(changePasswordRequest);
    }

    @Test
    public void getInfoAppsURLAndExpectDataIsExists(){
        List<PartnerUrl> partnerUrlList = new ArrayList<>();

        PartnerUrl firstPartnerUrl = new PartnerUrl();
        firstPartnerUrl.setUrlId(1);
        firstPartnerUrl.setServiceName("first_service");
        firstPartnerUrl.setBaseUrl("http://localhost:8080");
        firstPartnerUrl.setEndpointUrl("/first/endpoint-url/sample");

        partnerUrlList.add(firstPartnerUrl);

        PartnerUrl secondPartnerUrl = new PartnerUrl();
        secondPartnerUrl.setUrlId(1);
        secondPartnerUrl.setServiceName("second_service");
        secondPartnerUrl.setBaseUrl("http://localhost:8080");
        secondPartnerUrl.setEndpointUrl("/second/endpoint-url/sample");

        partnerUrlList.add(secondPartnerUrl);

        InfoAppDetail infoAppDetail = new InfoAppDetail();
        infoAppDetail.setUrlDetailId(1);
        infoAppDetail.setApkVersion("v1.0.1");
        infoAppDetail.setDescription("info_app_detail_1");

        List<InfoAppDetail> infoAppDetails = new ArrayList<>();
        infoAppDetails.add(infoAppDetail);

        when(infoAppDetailRepository.findAllInfoAppDetail()).thenReturn(infoAppDetails);
        when(partnerUrlRepository.findAllSortedById()).thenReturn(partnerUrlList);

        VersionDTO versionDTO = idmService.getInfoApps();
        assertTrue(versionDTO.getPartnerUrls().size() == 2);
    }

    @Test
    public void getInfoAppsURLWhenPathURLIsNullAndExpectPathURLListIsEmpty(){

        InfoAppDetail infoAppDetail = new InfoAppDetail();
        infoAppDetail.setUrlDetailId(1);
        infoAppDetail.setApkVersion("v1.0.1");
        infoAppDetail.setDescription("info_app_detail_1");

        List<InfoAppDetail> infoAppDetails = new ArrayList<>();
        infoAppDetails.add(infoAppDetail);

        when(infoAppDetailRepository.findAllInfoAppDetail()).thenReturn(infoAppDetails);
        when(partnerUrlRepository.findAllSortedById()).thenReturn(null);

        VersionDTO versionDTO = idmService.getInfoApps();
        assertTrue(versionDTO.getPartnerUrls().size() == 0);

    }

    @Test
    public void findUserByIdV2TestWhenUserIsAXIOrKedayAndExpectGetUserInfo() throws AdiraCustomException, JsonProcessingException {
        userDetailV2.setUserType("AXI");
        userDetailV2.setPortfolio("AXI");

        userDetailDTOV2.setUserId("US002");
        userDetailDTOV2.setUserType("AXI");
        userDetailDTOV2.setDeleted(0);
        userDetailDTOV2.setIsactive(0);
        userDetailDTOV2.setPartnerCode("PA002");
        userDetailDTOV2.setNoWa("01234567890");
        userDetailDTOV2.setHandphone("01234567890");
        userDetailDTOV2.setCreateBy("Me");
        userDetailDTOV2.setId(1);
        userDetailDTOV2.setEmail("sample@adira.com");

        String[] strings = new String[2];
        strings[0] = "AXI";
        strings[1] = "005-AXI";

        when(userRepository.findUserByUserId("US002")).thenReturn(userDetailV2);
        when(modelMapper.map(userDetailV2, UserDetailDTOV2.class)).thenReturn(userDetailDTOV2);
        when(portfolioMappingUtil.portfolioMappingFromTable()).thenReturn(map);

        UserDetailDTOV2 response = idmService.findUserByIdV2("US002");
        assertTrue(response.getPortfolios().get(0).getObjectDesc().equals("AXI"));
    }

    @Test
    public void checkDataTestAndExpectDataExists() throws AdiraCustomException {
        LoginUserDetailDTO loginUserDetailDTO = new LoginUserDetailDTO();
        loginUserDetailDTO.setId(1);
        loginUserDetailDTO.setLastLoginDate(new Timestamp(1000));
        loginUserDetailDTO.setStillLogin(true);
        loginUserDetailDTO.setActiveStatus("1");
        loginUserDetailDTO.setFirstTime(true);
        loginUserDetailDTO.setLoginCount(1);
        loginUserDetailDTO.setUsername("first_username");

        List<LoginUserDetail> loginUserDetails = new ArrayList<>();

        LoginUserDetail firstLoginUserDetail = new LoginUserDetail();
        firstLoginUserDetail.setId(1);
        firstLoginUserDetail.setFirstTime(true);
        firstLoginUserDetail.setUsername("first_username");
        firstLoginUserDetail.setLastLoginDate(new Timestamp(1000));
        firstLoginUserDetail.setActiveStatus("1");
        firstLoginUserDetail.setStillLogin(true);
        firstLoginUserDetail.setCreatedBy("Me");
        firstLoginUserDetail.setLastModifiedBy("Me");
        firstLoginUserDetail.setLoginCount(1);

        loginUserDetails.add(firstLoginUserDetail);

        when(loginUserDetailRepository.findByUsername("first_username")).thenReturn(loginUserDetails);
        when(modelMapper.map(firstLoginUserDetail, LoginUserDetailDTO.class)).thenReturn(loginUserDetailDTO);

        Map<String, Object> result = idmService.checkData("first_username");
        assertEquals(result.get(AUTH_FIRST_LOGIN.toString()), true);
    }

    @Test
    public void checkDataTestWhenFirstLoginAndSaveNewUsername() throws AdiraCustomException {
        List<LoginUserDetail> loginUserDetails = new ArrayList<>();

        LoginUserDetailDTO loginUserDetailDTO = new LoginUserDetailDTO();
        loginUserDetailDTO.setId(1);
        loginUserDetailDTO.setLastLoginDate(new Timestamp(1000));
        loginUserDetailDTO.setStillLogin(true);
        loginUserDetailDTO.setActiveStatus("1");
        loginUserDetailDTO.setFirstTime(true);
        loginUserDetailDTO.setLoginCount(1);
        loginUserDetailDTO.setUsername("first_username");

        LoginUserDetail firstLoginUserDetail = new LoginUserDetail();
        firstLoginUserDetail.setId(1);
        firstLoginUserDetail.setFirstTime(true);
        firstLoginUserDetail.setUsername("first_username");
        firstLoginUserDetail.setLastLoginDate(new Timestamp(1000));
        firstLoginUserDetail.setActiveStatus("1");
        firstLoginUserDetail.setStillLogin(true);
        firstLoginUserDetail.setCreatedBy("Me");
        firstLoginUserDetail.setLastModifiedBy("Me");
        firstLoginUserDetail.setLoginCount(1);

        when(loginUserDetailRepository.findByUsername("first_username")).thenReturn(loginUserDetails);
        when(modelMapper.map(firstLoginUserDetail, LoginUserDetailDTO.class)).thenReturn(loginUserDetailDTO);

        Map<String, Object> result = idmService.checkData("first_username");
        assertEquals(result.get(AUTH_FIRST_LOGIN.toString()), true);
    }

    private String mapJsonResponseToString(){
        return "\"{\\\"Status\\\":0,\\\"Message\\\":\\\"Success\\\", \\\"Data\\\":[{\\\"OBJT_CODE\\\":\\\"0\\\",\\\"OBJT_DESC\\\":\\\"<--SELECT ONE-->\\\"},{\\\"OBJT_CODE\\\":\\\"004\\\",\\\"OBJT_DESC\\\":\\\"MOBIL BEKAS         \\\"}]}\"";
    }

    private String mapDataResponseToString(){
        return "\"{\\\"Data\\\":[{\\\"OBJT_CODE\\\":\\\"0\\\",\\\"OBJT_DESC\\\":\\\"<--SELECT ONE-->\\\"},{\\\"OBJT_CODE\\\":\\\"004\\\",\\\"OBJT_DESC\\\":\\\"MOBIL BEKAS         \\\"}]}\"";
    }

}
